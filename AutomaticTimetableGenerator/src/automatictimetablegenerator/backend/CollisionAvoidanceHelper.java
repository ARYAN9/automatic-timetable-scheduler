/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.backend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Aryan Dhami
 */
public class CollisionAvoidanceHelper {

    private int alloted[][];    
    private static int lectureAlloted[][] = new int[8][6];
    private Teacher teacherObj;
    private int totalFreeLecturesCount = 48;
    private final ArrayList<Integer> rowList;
    private final ArrayList<Integer> columnList;    
    
    public CollisionAvoidanceHelper(Teacher teacherObj) {
        this.teacherObj = teacherObj;
        this.alloted = new int[8][6];
        this.alloted = teacherObj.getLectureAllocated();
        this.totalFreeLecturesCount = teacherObj.getTotalLecturesRemaining();
        //The below both are for taking same lectures of teachers in the same slot!!!
        this.columnList = new ArrayList<>();
        this.rowList = new ArrayList<>();
//        free = new int[8][6];
//        //In start all are free!!!
//        for(int i=0;i<8;i++){
//            for(int j=0;j<6;j++){
//                free[i][j] = 1;
//            }
//        }
    }
    
    /**
     * The below copy constructor is for creating the deep copy!!!
     * @param collisionAvoidanceHelperObj : The reference of the object being deep copied!!!
     */
    public CollisionAvoidanceHelper(CollisionAvoidanceHelper collisionAvoidanceHelperObj){
        this(new Teacher(collisionAvoidanceHelperObj.getTeacherObj().getName()));
    }
    
    public void setLectureAlloted(int row,int column){
        lectureAlloted[row][column] = 1;
        this.alloted[row][column] = 1;
//        this.free[row][column] = 0;        
//        this.totalFreeLecturesCount--;
    }
    
    public void removeLectureAlloted(int row,int column){
        lectureAlloted[row][column] = 0;
        this.alloted[row][column] = 0;
//        this.free[row][column] = 1;        
//        this.totalFreeLecturesCount++;
    }
    
    public boolean isLectureFree(int row,int column){
        if(lectureAlloted[row][column] == 1)
            return false;
        return true;
    }

    public boolean isTeacherLectureFree(int row,int column){
        if(this.alloted[row][column] == 1)
            return false;
        return true;
    }
    
    public boolean isTeacherLectureAlloted(int row,int column){
        if(this.alloted[row][column] == 0)
            return false;
        return true;
    }
    
    public int getTotalFreeLecturesCount(){
        return totalFreeLecturesCount;
    }
    
    public void decrementTotalFreeLecturesCount(){
        this.totalFreeLecturesCount--;
    }
    
    public void increamentTotalFreeLecturesCount(){
        this.totalFreeLecturesCount++;
    }
    
    public int uniqueFreeLecturesCount(CollisionAvoidanceHelper helperObj){
        HashSet<String> set = new HashSet();  

        for(int i=0;i<8;i++){
            for(int j=0;j<6;j++){
                if(this.alloted[i][j] == 0){
                    set.add(i+""+j);
                }                
            }
        }
        
        for(int i=0;i<8;i++){
            for(int j=0;j<6;j++){
                if(helperObj.alloted[i][j] == 0){
                    set.add(i+""+j);
                }                
            }
        }
        return set.size();
    }
    
    //------
    
    public Teacher getTeacherObj(){
        return this.teacherObj;
    }
    
    public static HashMap<String,CollisionAvoidanceHelper> deepCopy(Map<String,CollisionAvoidanceHelper> original)
    {
        HashMap<String,CollisionAvoidanceHelper> copy = new HashMap<>();
//        for (String key : original.keySet())
//        {
//            copy.put(key,new CollisionAvoidanceHelper(original.get(key)));
//        }
        original.keySet().stream().forEach((key) -> {
            copy.put(key,new CollisionAvoidanceHelper(original.get(key)));
        });
        return copy;
    }

    public ArrayList<Integer> getRowList() {
        return rowList;
    }

    public ArrayList<Integer> getColumnList() {
        return columnList;
    }
    //------
}
