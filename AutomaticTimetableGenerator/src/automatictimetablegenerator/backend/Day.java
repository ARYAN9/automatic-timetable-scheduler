/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.backend;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 *
 * @author Aryan Dhami
 */
public class Day {
    //Every Day class object will have lectures which is Array and it contains the 6 lectures of that Day!!!
    //Not used LinkedHashMap because it is costlier as compared to Array!!!
    private Lecture lectures[];
//    private ArrayList<Lecture> lectures;
    public Day() {
//        lectures = new ArrayList<>();
//        lectures = new Lecture[6];
        lectures = new Lecture[8];//6+2extra lec
//        for(int i=0;i<8;i++)
//            lectures[i] = new Lecture();
    }
    
//    public void addLecture(int lectureNo,Lecture lecture){
////        this.lectures[lectureNo] = lecture;
//        this.lectures[lectureNo] = lecture;
//    }
    public void addThoeryLecture(int lectureNo,TheoryLecture lecture){
//        this.lectures[lectureNo] = new TheoryLecture(name);
        this.lectures[lectureNo] = lecture;
    }
    
    public void addPracticalLecture(int lectureNo,PraticalLecture lecture){
//        this.lectures[lectureNo] = new PraticalLecture(aBatchName, bBatchName, cBatchName);
        this.lectures[lectureNo] = lecture;
    }

    public Lecture getLecture(int lectureNo) {
        return this.lectures[lectureNo];
//        return this.lectures.get(lectureNo);
    }    
}
