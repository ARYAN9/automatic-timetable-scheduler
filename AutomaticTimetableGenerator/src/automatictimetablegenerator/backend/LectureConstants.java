/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.backend;

/**
 *
 * @author Aryan Dhami
 */
public interface LectureConstants {
    int NO_LECTURE = 0;
    int LECTURE = 1;
    int PRACTICALS = 2;
}
