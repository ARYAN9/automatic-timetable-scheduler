/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.backend;

import automatictimetablegenerator.ui.Timetable;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Aryan Dhami
 */
public class MainTimetable {
    private ArrayList<Subject> subjects;
    private Day timeTable[];
    private int filled[][];
    private int semesterNumber,numberOfWeeks;
//    private String branchName;
    private final int branchId;
    private Map<Subject,Teacher> mappingOfSubjectToTeacherForBatch2,mappingOfSubjectToTeacherForBatch3;

//    private ConcurrentHashMap<String,Teacher> teachers;
//    private final HashSet<String> allBranches;
//    private final Map<String, Teacher> teachers;
    
//    private ConcurrentHashMap<String,Teacher> oddTeachers;
    
    public MainTimetable(int semesterNumber,int numberOfWeeks,int branchId,ArrayList<Subject> subjects,Map<Subject,Teacher> mappingOfSubjectToTeacherForBatch2,Map<Subject,Teacher> mappingOfSubjectToTeacherForBatch3) {
        this.semesterNumber = semesterNumber;
        this.numberOfWeeks = numberOfWeeks;
        this.branchId = branchId;
        this.subjects = subjects;
        this.mappingOfSubjectToTeacherForBatch2 = mappingOfSubjectToTeacherForBatch2;
        this.mappingOfSubjectToTeacherForBatch3 = mappingOfSubjectToTeacherForBatch3;
        System.out.println("mappingOfSubjectToTeacherForBatch2 is:"+mappingOfSubjectToTeacherForBatch2);
        System.out.println("mappingOfSubjectToTeacherForBatch3 is:"+mappingOfSubjectToTeacherForBatch3);
//        this.teachers = teachers;
        this.filled = new int[8][6];
        
//        System.out.println("Printing subjects");
//        for(Subject subject : subjects){
//            System.out.println("Name:"+subject.getName());
//            System.out.println("Thoery:"+subject.getTheoryHours());
//            System.out.println("pratucak:"+subject.getPraticalHours());
//        }
        initializeBlankTimeTable();
        generateTimetable();
    }
    
//    private void initializeAllTeachers(){
////            CO 1
////            CO 3
////        new XMLParser();
//        if((semesterNumber & 1) == 1){
//            //Means the semesterNumber is odd!!!
//            //            IntStream intStream = IntStream.iterate(1, i -> i + 2).limit(3).filter(i -> i != semesterNumber)
//            for(int i=1; i<6 && (i != semesterNumber); i=i+2){
////                    allBranches.forEach(i -> System.out.println(i)); 
//                for(String branch : allBranches){
//                    new XMLParser("./src/automatictimetablegenerator/"+branch+i+".xml").parseTimeTable(teachers);                                            
//                }
//            }
//        }else{
//            //Means the semesterNumber is even!!!
//            for(int i=2; i<=6 && (i != semesterNumber); i=i+2){
//                for(String branch : allBranches){
//                    new XMLParser("./src/automatictimetablegenerator/"+branch+i+".xml").parseTimeTable(teachers);                                            
//                }
//            }
//        }
//        
//    }
    
    private void initializeBlankTimeTable(){
        this.timeTable = new Day[6];
        /**
         * So every timeTable has 6 days and every 6 days has 6 lectures!!!
         */
        for(int i=0;i<6;i++){
            timeTable[i] = new Day();
        }
    }
    
    /**
     * The below function will take the batchName and noOfBatches return pending pending subject of that batch!!!
     * @param batchName : Integer value indicating it is 1,2 or 3 batch 
     *                    because only the subject having 'N' batches can only be associated with subject of 'N' batches!!!
     * @param noOfBatches
     * @return 
     */
    private HashSet<Subject> giveSubjectsPendingByBatch(char batchName,int noOfBatches,int j,int k){
        HashSet<Subject> pendingSubjects = new HashSet<>();
//        for(Subject subject: subjects){
//            if(batchName == 'c' && subject.getNoOfBatches() == noOfBatches){
//                if(subject.getcBatch() != 0)
//                    pendingSubjects.add(subject);
//            }else if(batchName == 'b' && subject.getbBatch() != 0 && subject.getNoOfBatches() == noOfBatches){
//                pendingSubjects.add(subject);
//            }
//        }
        //Jiske teacher ka j,k free hai vo hi return karo
        subjects.stream().forEach((subject) -> {
            Teacher teacherObj = mappingOfSubjectToTeacherForBatch3.get(subject);
            if(batchName == 'c' && subject.getNoOfBatches() == noOfBatches && teacherObj.getLectureAllocatedValue(j, k) == 0 && teacherObj.getLectureAllocatedValue(j+1, k) == 0){
                if(subject.getcBatch() != 0)
                    pendingSubjects.add(subject);
            }else if(batchName == 'b' && subject.getbBatch() != 0 && subject.getNoOfBatches() == noOfBatches){
                pendingSubjects.add(subject);
            }
        });
        return pendingSubjects;
    }
    
    private boolean getIsMultipleLecture(int numBatches){
        ArrayList<Subject> allSubjects = new ArrayList<>();
        int counter = 0;
        for(Subject subjectObj : subjects) {
            if(subjectObj.getNoOfBatches() == numBatches){
                if(subjectObj.getaBatch() != 0 || subjectObj.getbBatch() != 0 || subjectObj.getcBatch() != 0){
                    counter++;
                }                
            }
        }
        if(counter >= numBatches)
            return true;
        return false;

//        return !(subjectObj.getaBatch() != 0 || subjectObj.getbBatch() != 0 || subjectObj.getcBatch() != 0);
    }
    
    private void generateTimetable(){
        ArrayList<Subject> batch1Subjects = new ArrayList<>();
        ArrayList<Subject> batch2Subjects = new ArrayList<>();
        ArrayList<Subject> batch3Subjects = new ArrayList<>();
        
        HashMap<String,Subject> getSubjectByName = new HashMap<>();
        int batch1PraticalSubjectsCounter,batch2PraticalSubjectsCounter,batch3PraticalSubjectsCounter,multipleTeacherHours;
        batch1PraticalSubjectsCounter = batch2PraticalSubjectsCounter = batch3PraticalSubjectsCounter = 0;
        /*Local variables initializations is over!!!*/
//        ArrayList<Subject> batch1Subjects123 = new ArrayList<>(mappingOfSubjectToTeacherForBatch2.keySet());
        /**
         * Now, we are looping through all subjects and splitting each subject according to his respective batch!!!
         */
        for(Subject subject : subjects){
            getSubjectByName.put(subject.getName(),subject);
            switch (subject.getNoOfBatches()) {
                case 1:
                    batch1Subjects.add(subject);
                    batch1PraticalSubjectsCounter = batch1PraticalSubjectsCounter + subject.getPraticalHours();
                    break;
                case 2:                    
                    batch2Subjects.add(subject);
//                    mappingOfSubjectToTeacherForBatch2.put(subject, subject.getTeachers().get(new Random().nextInt(subject.getTeachers().size())));
                    batch2PraticalSubjectsCounter = batch2PraticalSubjectsCounter + subject.getPraticalHours();
                    break;
                default:
                    batch3Subjects.add(subject);
//                    int index = 2;//It will have at least 3 teachers!!!So, index 0,1,2 !!!
//                    mappingOfSubjectToTeacherForBatch3.put(subject, subject.getTeachers().get(new Random().nextInt(subject.getTeachers().size())));
                    batch3PraticalSubjectsCounter = batch3PraticalSubjectsCounter + subject.getPraticalHours();
                    break;                    
            }
        }
        
        /*Performing right shift to divide by two on each counter!!!*/
        batch1PraticalSubjectsCounter = batch1PraticalSubjectsCounter >> 1;//Dividing by two because pratical in pair to two!!![So,divide by 2 rounds]
        batch2PraticalSubjectsCounter = batch2PraticalSubjectsCounter >> 1;//Dividing by two because pratical in pair to two!!![So,divide by 2 rounds]
        batch3PraticalSubjectsCounter = batch3PraticalSubjectsCounter >> 1;//Dividing by two because pratical in pair to two!!![So,divide by 2 rounds]
        
        //3 batch subjects processing!!!        
        multipleTeacherHours = 0;
        if(batch3Subjects.size() >= 3)
            multipleTeacherHours = batch3PraticalSubjectsCounter - (batch3PraticalSubjectsCounter % 3);
        else{
            System.out.println("Error hai:"+batch3Subjects);
            System.out.println("Error hai1:"+batch3Subjects.size());            
            System.out.println("Error hai tp:"+(batch3Subjects.size() >= 3));            
        }
        
        
        /**
         * Re-setting the primary teacher if the same teacher is found in another subject!!!
         */
        HashMap<Subject,Teacher> mappingOfSubjectToTeacherForBatch3Practical = new HashMap<>();
        HashSet<Teacher> teachersSetOfBatch3 = new HashSet<>();
        
        batch3Subjects.stream().map((subjectObj) -> {
            Teacher teacherObj = mappingOfSubjectToTeacherForBatch3.get(subjectObj);
            if(teachersSetOfBatch3.add(teacherObj))
            {
                mappingOfSubjectToTeacherForBatch3Practical.put(subjectObj, teacherObj);                
            }else{
                for(Teacher newTeacherObj:subjectObj.getTeachers())
                {
                    if(!newTeacherObj.equals(teacherObj))
                        mappingOfSubjectToTeacherForBatch3Practical.put(subjectObj, newTeacherObj);
                }
            }
            return subjectObj;
        }).forEach((subjectObj) -> {
            System.out.println("Subject Name:"+subjectObj.getName()+" And Teacher Name:"+mappingOfSubjectToTeacherForBatch3.get(subjectObj).getName());
        });
        
        while(batch3PraticalSubjectsCounter != 0){             
//            multipleTeacherHours = ;
//            if(multipleTeacherHours <= 0){//batch3Subjects.size() < 3
            if(!getIsMultipleLecture(3)){
                //Now, all will have the same teacher!!!
                boolean flag = false;
                
                for(Subject subject: batch3Subjects)
                {
                    if(subject.getaBatch() > 0 && subject.getbBatch() > 0 && subject.getcBatch() > 0){
                        /*The subject has A,B as well as C batch remaining*/
                        for(int j=0;j<6;j+=2){
                            for(int k=0;k<6;k++){
                                if(filled[j][k] == 0 && filled[j+1][k] == 0 && checkSpecialPraticalLecturesCondition(subject,j,k)){
                                    allotSpecialPraticalLectures(subject,j,k);
                                    flag = true;
                                    break;
                                }
                            }
                            if(flag)
                                break;
                        }
                        
                       if(!flag){
                            /*Means there is no space left now this has to alloted in the Extra Lectures!!!*/
                            for(int j=6;j<8;j++){
//                            int j = 6;
                                for(int k=0;k<6;k++){
                                    if(filled[j][k] == 0 && filled[j+1][k] == 0 && checkSpecialPraticalLecturesCondition(subject,j,k)){
                                        allotSpecialPraticalLectures(subject,j,k);
                                        flag = true;
                                        break;
                                    }
                                }
                                if(flag)
                                    break;
                            }
                        }
                    }
                }
                
//                for(int j=0;j<6;j+=2){
//                    for(int k=0;k<6;k++){
//                        System.out.println("Inside: j:"+j+" k:"+k);
//                        if(allotPraticalLectures(batch3Subjects,mappingOfSubjectToTeacherForBatch3,j,k,true)){//true is indicating all lectures will have same teachers!!!
//                            System.out.println("Condition inside: j:"+j+" k:"+k);
//                            flag = true;
//                            break;
//                        }
//                    }
//                    if(flag){
//                        System.out.println("Outer loop breaked");
//                        break;                        
//                    }
//                }
                System.out.println("If khatam");
            }else{
                System.out.println("Else mai aaya bhai mul:"+multipleTeacherHours);
                /*Here we need three teachers for three batches in a lecture!!!*/

                boolean flag = false;
                for(int j=0;j<6;j+=2){
                    for(int k=0;k<6;k++){
                        System.out.println("Main inside:"+batch3Subjects.size());
//                        System.out.println("singleTeacherHours:"+singleTeacherHours+"And batch3PraticalSubjectsCounter:"+batch3PraticalSubjectsCounter);
                        if(allotPraticalLectures(new ArrayList<>(batch3Subjects),mappingOfSubjectToTeacherForBatch3Practical,j,k)){
                            flag = true;
                            break;
                        }
                    }
                    if(flag)
                        break;
                }
                //
                
                for(Subject subject: batch3Subjects){
                    ArrayList<Integer> rowList = subject.getPracticalLectureRowList();
                    ArrayList<Integer> columnList = subject.getPracticalLectureColumnList();
                    for(int i=0;i<rowList.size();i++){
                        if(allotPraticalLectures(new ArrayList<>(batch3Subjects),mappingOfSubjectToTeacherForBatch3Practical,rowList.get(i),columnList.get(i))){
                            
                        }else{
                            System.out.println("Their is some major problem");
                        }
                    }
                }
                
                //
                
                if(!flag){
                    /*Means there is no space left now this has to alloted in the Extra Lectures!!!*/
                    for(int j=6;j<8;j++){
                        for(int k=0;k<6;k++){
                            if(allotPraticalLectures(new ArrayList<>(batch3Subjects),mappingOfSubjectToTeacherForBatch3Practical,j,k)){
                                flag = true;
                                break;
                            }
                        }
                        if(flag)
                            break;
                    }
                }
                /*Batch 3 subjects are been alloted!!!*/
            }//end of else!!!
            batch3PraticalSubjectsCounter--;
            multipleTeacherHours--;
        }//end of while!!!//
        
        //All subjects pratical lectures having 3 batches are done!!!
        multipleTeacherHours = 0;
        if(batch2Subjects.size() >= 2)
            multipleTeacherHours = batch2PraticalSubjectsCounter - (batch2PraticalSubjectsCounter % 2);
        int timepass = 0;
        System.out.println("==============MAIN LIST===========================");
        HashSet<Teacher> teachersSetOfBatch2 = new HashSet<>();
        HashMap<Subject,Teacher> mappingOfSubjectToTeacherForBatch2Practical = new HashMap<>();
        /**
         * Re-setting the primary teacher if the same teacher is found in another subject!!!
         */
        batch2Subjects.stream().map((subjectObj) -> {
            Teacher teacherObj = mappingOfSubjectToTeacherForBatch2.get(subjectObj);
            if(teachersSetOfBatch2.add(teacherObj))
            {
                mappingOfSubjectToTeacherForBatch2Practical.put(subjectObj, teacherObj);                
            }else{
                for(Teacher newTeacherObj:subjectObj.getTeachers())
                {
                    if(!newTeacherObj.equals(teacherObj))
                        mappingOfSubjectToTeacherForBatch2Practical.put(subjectObj, newTeacherObj);
                }
            }
            return subjectObj;
        }).forEach((subjectObj) -> {
            System.out.println("Subject Name:"+subjectObj.getName()+" And Teacher Name:"+mappingOfSubjectToTeacherForBatch2.get(subjectObj).getName());
        });
        
        System.out.println("Main final ans");
        for(Subject subjectObj : mappingOfSubjectToTeacherForBatch2Practical.keySet()){
            System.out.println("Subject Name:"+subjectObj+" And Teacher Name:"+mappingOfSubjectToTeacherForBatch2Practical.get(subjectObj));
        }
        
        while(batch2PraticalSubjectsCounter != 0){
            System.out.println("Loop:"+timepass++);
            System.out.println("");
//            if(multipleTeacherHours <= 0){//|| batch2Subjects.size() < 2
            if(!getIsMultipleLecture(2)){
                //Now, all will have the same teacher!!!
                boolean flag = false;
                
                for(Subject subject: batch2Subjects)
                {
                    if(subject.getaBatch() > 0 && subject.getbBatch() > 0){
                        /*The subject has A as well as B batch remaining*/
                        for(int j=0;j<6;j+=2){
                            for(int k=0;k<6;k++){
                                if(filled[j][k] == 0 && filled[j+1][k] == 0 && checkSpecialPraticalLecturesCondition(subject,j,k)){
                                    allotSpecialPraticalLectures(subject,j,k);
                                    flag = true;
                                    break;
                                }
                            }
                            if(flag)
                                break;
                        }
                        
                       if(!flag){
                            /*Means there is no space left now this has to alloted in the Extra Lectures!!!*/
                            for(int j=6;j<8;j++){
//                            int j = 6;
                                for(int k=0;k<6;k++){
                                    if(filled[j][k] == 0 && filled[j+1][k] == 0 && checkSpecialPraticalLecturesCondition(subject,j,k)){
                                        allotSpecialPraticalLectures(subject,j,k);
                                        flag = true;
                                        break;
                                    }
                                }
                                if(flag)
                                    break;
                            }
                        }
                    }
                }
                
                
                /*Here we need two teachers for two batches in a lecture!!!*/
//                boolean flag = false;
//                for(int j=0;j<6;j+=2){
//                    for(int k=0;k<6;k++){
//                        if(allotPraticalLectures(new ArrayList<Subject>(batch2Subjects),mappingOfSubjectToTeacherForBatch2,j,k)){
//                            System.out.println("Fianlly Alloted:["+j+k+"]");
//                            flag = true;
//                            break;
//                        }
//                    }
//                    if(flag)
//                        break;
//                }
//                if(!flag){
//                    /*Means there is no space left now this has to alloted in the Extra Lectures!!!*/
//                    for(int j=6;j<8;j++){
//                        for(int k=0;k<6;k++){
//                            if(allotPraticalLectures(new ArrayList<Subject>(batch2Subjects),mappingOfSubjectToTeacherForBatch2,j,k)){
//                                flag = true;
//                                break;
//                            }
//                        }
//                        if(flag)
//                            break;
//                    }
//                }
            }else{                   
                boolean flag = false;
                for(int j=0;j<6;j+=2){
                    for(int k=0;k<6;k++){
                        if(allotPraticalLectures(new ArrayList<Subject>(batch2Subjects),mappingOfSubjectToTeacherForBatch2Practical,j,k)){
                            flag = true;
                            break;
                        }
                    }
                    if(flag)
                        break;
                }
                if(!flag){
                    /*Means there is no space left now this has to alloted in the Extra Lectures!!!*/
                    for(int j=6;j<8;j++){
                        for(int k=0;k<6;k++){
                            if(allotPraticalLectures(new ArrayList<Subject>(batch2Subjects),mappingOfSubjectToTeacherForBatch2Practical,j,k)){
                                flag = true;
                                break;
                            }
                        }
                        if(flag)
                            break;
                    }
                }
                /*Batch 2 subjects are been alloted!!!*/
            }//end of else!!!
            batch2PraticalSubjectsCounter--;
            multipleTeacherHours--;
        }//end of while!!!
        //All subjects pratical lectures having 2 batches are done!!!

//        multipleTeacherHours = batch1PraticalSubjectsCounter;
        while(batch1PraticalSubjectsCounter != 0){       
//            if(multipleTeacherHours == 0){
//                /*Here we need three teachers for three batches in a lecture!!!*/
////                allotForSpecialPraticalLectures();
//                boolean flag = false;
//                for(int j=0;j<6;j+=2){
//                    for(int k=0;k<6;k++){
//                        if(allotPraticalLectures(new ArrayList<Subject>(batch1Subjects),null,j,k)){
//                            flag = true;
//                            break;
//                        }
//                    }
//                    if(flag)
//                        break;
//                }
//                if(!flag){
//                    /*Means there is no space left now this has to alloted in the Extra Lectures!!!*/
//                    for(int j=6;j<8;j++){
//                        for(int k=0;k<6;k++){
//                            if(allotPraticalLectures(new ArrayList<Subject>(batch1Subjects),null,j,k)){
//                                flag = true;
//                                break;
//                            }
//                        }
//                        if(flag)
//                            break;
//                    }
//                }
//            }else
            {                   
                boolean flag = false;
                for(int j=0;j<6;j+=2){
                    for(int k=0;k<6;k++){
                        if(allotPraticalLectures(new ArrayList<Subject>(batch1Subjects),null,j,k)){
                            flag = true;
                            break;
                        }
                    }
                    if(flag)
                        break;
                }
                if(!flag){
                    /*Means there is no space left now this has to alloted in the Extra Lectures!!!*/
                    for(int j=6;j<8;j++){
                        for(int k=0;k<6;k++){
                            if(allotPraticalLectures(new ArrayList<Subject>(batch1Subjects),null,j,k)){
                                flag = true;
                                break;
                            }
                        }
                        if(flag)
                            break;
                    }
                }
                /*Batch 3 subjects are been alloted!!!*/

//                checkForExtraPraticalLectures();                                    
            }//end of else!!!
            batch3PraticalSubjectsCounter--;
//            multipleTeacherHours--;
        }//end of while!!!
        
        /*Just traversing the timetable*/
//        for(int i=0;i<6;i++){
//            for(int j=0;j<8;j++){                            
////                System.out.println("Day"+i+" :"+((Day)timeTable[i]).getLecture(j)+ " Lecture Type is theory: "
////                        +((Day)timeTable[i]).getLecture(j).isIsTheory());                            
//                System.out.println("Day"+i+" :"+((Day)timeTable[i]).getLecture(j));                            
//
//            }
//        }
//        HashMap<String,Teacher> mappingOfSubjectToTeacher = new HashMap<>();
//        for(int i=0;i<6;i++){
//            for(int j=0;j<8;j++){                            
//                System.out.println("Day"+i+" :"+((Day)timeTable[i]).getLecture(j));                            
//                if(((Day)timeTable[i]).getLecture(j) != null){
//                    if(((Day)timeTable[i]).getLecture(j).getLectureType() == LectureConstants.PRACTICALS){
//                        PraticalLecture praticalLecture = (PraticalLecture)((Day)timeTable[i]).getLecture(j);
//                        ArrayList<Teacher> teachers = praticalLecture.getTeachers();
//                        if(!"".equals(praticalLecture.getBBatchName())){
//                            Teacher aBatchTeacher = teachers.get(0);
//                            Teacher bBatchTeacher = teachers.get(1);
//                            if(!"".equals(praticalLecture.getCBatchName())){
//                                Teacher cBatchTeacher = teachers.get(2);
//                                System.out.println("In too aayaa hai bhai!!!");
//                                /*There is A,B and C batch!!!*/                                
//                                if(aBatchTeacher.equals(bBatchTeacher) && bBatchTeacher.equals(cBatchTeacher)){
//                                    /*All 3 Batch teachers are same!!!*/
//                                    /*Now,changed the B batch teacher!!!*/
//                                    Teacher newBBatchTeacher = null;
//                                    for(Teacher teacherObj : getSubjectByName.get(praticalLecture.getBBatchName()).getTeachers()){
//                                        if((!teacherObj.equals(bBatchTeacher))){// && (!teacherObj.equals(aBatchTeacher))){
//                                            praticalLecture.setTeacher(teacherObj,1);
//                                            newBBatchTeacher = teacherObj;
//                                            System.out.println("New B batche teacher is changed and it is:"+teacherObj.getName());
//                                            break;
//                                        }
//                                    }
//
//                                    /*Now,changed the C batch teacher!!!*/
//                                    for(Teacher teacherObj : getSubjectByName.get(praticalLecture.getBBatchName()).getTeachers()){
//                                        if((!teacherObj.equals(newBBatchTeacher)) && (!teacherObj.equals(aBatchTeacher))){
//                                            System.out.println("New C batche teacher is changed and it is:"+teacherObj.getName());
//                                            praticalLecture.setTeacher(teacherObj,2);
//                                            break;
//                                        }
//                                    }                                
//                                }else if(aBatchTeacher.equals(bBatchTeacher) || bBatchTeacher.equals(cBatchTeacher)){
//                                    /*Means A batch and B batch teacher same or B batch or C batch teacher same!!!*/
//                                    /*Now,changed the B batch teacher!!!*/
//                                    System.out.println("Inside hai dost!!!!");
//                                    for(Teacher teacherObj : getSubjectByName.get(praticalLecture.getBBatchName()).getTeachers()){
//                                        if(!(teacherObj.equals(bBatchTeacher) || aBatchTeacher.equals(teacherObj) || cBatchTeacher.equals(teacherObj))){
//                                            System.out.println("New B batche teacher is changed and it is12345:"+teacherObj.getName()+
//                                                    " For this Subject:"+praticalLecture.getBBatchName());
//                                            praticalLecture.setTeacher(teacherObj,1);
//                                            mappingOfSubjectToTeacher.put(praticalLecture.getBBatchName(),teacherObj);
//                                            System.out.println("Answer is:"+praticalLecture.getTeachers().get(1));
//                                            PraticalLecture praticalLecture1 = (PraticalLecture)((Day)timeTable[i]).getLecture(j);
//                                            System.out.println("Answer1 is:"+praticalLecture1.getTeachers().get(1));
//                                            break;
//                                        }
//                                    }
//                                }
//                            }else if(aBatchTeacher.equals(bBatchTeacher)){
//                                /*There is only A and B batch!!!*/
////                                {   
//                                    /*Now,changed the B batch teacher!!!*/
//                                    for(Teacher teacherObj : getSubjectByName.get(praticalLecture.getBBatchName()).getTeachers()){
//                                        if(!teacherObj.equals(bBatchTeacher)){
//                                            System.out.println("New B batche teacher is changed and it is 123:"+teacherObj.getName());
//                                            praticalLecture.setTeacher(teacherObj,1);
//                                            mappingOfSubjectToTeacher.put(praticalLecture.getBBatchName(),teacherObj);
//                                            break;
//                                        }
//                                    }
//                                    /*
//                                    getSubjectByName.get(praticalLecture.getBBatchName()).getTeachers().stream().filter((teacherObj) -> (!teacherObj.equals(bBatchTeacher))).map((teacherObj) -> {
//                                        praticalLecture.setTeacher(teacherObj,1);
//                                        return teacherObj;
//                                    }).forEach((teacherObj) -> {
//                                        mappingOfSubjectToTeacher.put(praticalLecture.getBBatchName(),teacherObj);
//                                    });
//
//                                    */
////                                }
//                            }
//                        }
//                    }
//                }                
//            }
//        }
        
        System.out.println("Final Traverse");
        for(int i=0;i<6;i++){
            for(int j=0;j<8;j++){                            
                System.out.println("Day"+i+" :"+((Day)timeTable[i]).getLecture(j));
            }
        }
        
        /*At this point all the prratical lectures of that subjectes has been alloted!!!*/
        for(Subject subject : subjects){
            while(subject.decrementTheoryHours()){
                Teacher teacherObj = null;
                int noOfBatches = subject.getNoOfBatches();
                if(noOfBatches == 2){
                    teacherObj = mappingOfSubjectToTeacherForBatch2.get(subject);
                }else if(noOfBatches == 3)
                    teacherObj = mappingOfSubjectToTeacherForBatch3.get(subject);
                else
                    teacherObj = subject.getTeachers().get(0);
                
                
//                Teacher teacherObj = subject.getTeacherObj();
                boolean flag = false;
                for(int j=5;j>=0;j--){
                    for(int k=5;k>=0;k--){
                        /*Checking if the lecture are free then allot it!!!*/
                        if(teacherObj.getLectureAllocatedValue(j, k) == 0 && filled[j][k] == 0){
                            filled[j][k] = 1;
                            teacherObj.setLectureAllocated(j,k);//00
                            ((Day)timeTable[k]).addThoeryLecture(j,new TheoryLecture(subject.getName(), teacherObj));
                            flag = true; //For breaking the outer loop!!!
                            break;
                        }
                    }
                    if(flag)
                        break;
                }
                if(!flag){
                    /*Means there is no space left now this has to alloted in the Extra Lectures!!!*/
                    for(int j=6;j<8;j++){
                        for(int k=0;k<6;k++){ 
                            /*Checking if the lecture are free then allot it!!!*/
                            if(teacherObj.getLectureAllocatedValue(j, k) == 0 && filled[j][k] == 0){
                                filled[j][k] = 1;
                                teacherObj.setLectureAllocated(j,k);//00
                                ((Day)timeTable[k]).addThoeryLecture(j,new TheoryLecture(subject.getName(), teacherObj));
//                                ((Day)timeTable[k]).addLecture(j,new Lecture(subject.getName(), true));
                                flag = true; //For breaking the outer loop!!!
                                break;
                            }
                        }
                        if(flag)
                            break;
                    }
                }
            }
        }
        
        if(branchId == 1)
            new XMLParser("./src/automatictimetablegenerator/CO"+semesterNumber+".xml").writeTimetable(timeTable);
        
        java.awt.EventQueue.invokeLater(() -> {
            new Timetable(timeTable).setVisible(true);
        });

//        new Timetable(timeTable);
    }    

//    private boolean checkForPraticalLectures(ArrayList<Subject> batchSubjects,Map<Subject,Teacher> mappingOfSubjectToTeacher,int j,int k,boolean isSpecial){
////        uniqueTeachersForBatch.get(0);
//        Random random = new Random();
//        Teacher diffCTeacher = null,diffBTeacher = null;
//        boolean flag = false,specialFlag = false;
//        System.out.println("Size:"+batchSubjects.size());
//        Collections.shuffle(batchSubjects);
//        /*Check if the batch3Subjects ArrayList size is minimum three!!!*/
//        /*Checking that lecture is not already alloted!!!*/
//        int index = 0;
//        if(filled[j][k] == 0 && filled[j+1][k] == 0){
//            /*Now, we need to choose 3 random subjects who has 3 pratical batches*/
//            Subject subjectA,subjectB,subjectC;
//            subjectA = subjectB = subjectC = null;
//            int noOfBatches = batchSubjects.get(0).getNoOfBatches();
//            while(true){
////                subjectA = batchSubjects.get(random.nextInt(batchSubjects.size()));
//                System.out.println("Index:"+index);
//                subjectA = batchSubjects.get(index++); // error line
//                System.out.println("SubjectA Name:"+subjectA.getName());
//                while(!subjectA.decrementABatchPracticalHours())
//                    subjectA = batchSubjects.get(index++);
//                
////                    subjectA = batchSubjects.get(random.nextInt(batchSubjects.size()));
//                System.out.println("SubjectA Name123:"+subjectA.getName());
//                
//                Teacher subjectATeacher = mappingOfSubjectToTeacher.get(subjectA);
//                
//                if(isSpecial){
//                    if(subjectA.getTeachers().get(0).getLectureAllocatedValue(j, k) == 1 || subjectA.getTeachers().get(0).getLectureAllocatedValue(j+1, k) == 1)
//                        subjectA.increamentABatchPracticalHours();
//                    else{
//                        batchSubjects.remove(--index);
////                        index++;
//                        break;                        
//                    }
//                }else if(subjectATeacher.getLectureAllocatedValue(j, k) == 1 || subjectATeacher.getLectureAllocatedValue(j+1, k) == 1){
//                    subjectA.increamentABatchPracticalHours();
//                    System.out.println("In subjectA condtion");
//                }
//                else{
//                    batchSubjects.remove(--index);
////                    index++;
//                    break;
//                }
//            }              
//            System.out.println("subjectA is finalized: subjectA is:"+subjectA.getName());
//            index = 0;//restarting
//            /*subjectA is finalized*/
//
//            if(noOfBatches >= 2){
//                while(true){
//                    System.out.println("Inifinite in b: isSpecial"+isSpecial+"j:"+j+"K:"+k);
//                    
//                    if(batchSubjects.size() > index){
//                        subjectB = batchSubjects.get(index++);
//                        while(!subjectB.decrementBBatchPracticalHours()){
//    //                        subjectB = batchSubjects.get(index++);
//                            System.out.println("This is the subject B selected:"+subjectB.getName()+" And its btach:"+subjectB.getbBatch()+"And batchsubject is:"+batchSubjects+"and index is:"+index);
//                            if(batchSubjects.size() > index)
//                                subjectB = batchSubjects.get(index++);
//                            else{
//                                Iterator<Subject> iterator = giveSubjectsPendingByBatch('b',noOfBatches).iterator();
//                                if(iterator.hasNext())
//                                    subjectB = iterator.next();
//                                System.out.println("Subject B changed1:"+subjectB.getName());
//                                specialFlag = true;
//                                index++;
//                                break;
//                            }
////                            subjectB.decrementBBatchPracticalHours();
//                            System.out.println("Index:"+index);
//                        }
//
//                    }else{
//                        Iterator<Subject> iterator = giveSubjectsPendingByBatch('b',noOfBatches).iterator();
//                        if(iterator.hasNext())
//                            subjectB = iterator.next();
//                        System.out.println("Subject B changed2:"+subjectB.getName());
//                        specialFlag = true;
//                        index++;
//                    }
//                    
////
//                                    
////
//
//                    if(isSpecial){//subjectB == subjectA || 
//                        if(subjectB.getTeachers().get(1).getLectureAllocatedValue(j, k) == 1 || subjectB.getTeachers().get(1).getLectureAllocatedValue(j+1, k) == 1)
//                            subjectB.increamentBBatchPracticalHours();
//                        else if(batchSubjects.size() > index){
//                            batchSubjects.remove(--index);
//                            index++;
//                            break;
//                        }else{
//                            break;
//                        }
//                    }else if(specialFlag){
//                        System.out.println("In specisal");
//                        System.out.println("SubjectB alag wala is:"+subjectB);
//                        System.out.println("SubjectA alag wala is:"+subjectA);
////                        Teacher teacherObj = mappingOfSubjectToTeacher.get(subjectB);
////                        if(teacherObj == mappingOfSubjectToTeacher.get(subjectA)){
////                            System.out.println("Undar if");
////                            /*So, now we have to take differnt teahcer*/
////                            for(Teacher teacher: subjectB.getTeachers()){
////                                if(!teacherObj.getName().equals(teacher.getName())){
////                                    if(teacher.getLectureAllocatedValue(j, k) == 0 && teacher.getLectureAllocatedValue(j+1, k) == 0){
////                                        diffBTeacher = teacher;
////                                        System.out.println("New teacher alloted");
////                                        break;
////                                    } 
////                                }
////                            }  
////                        }
//                        System.out.println("Outer loop breaked!!!");
//                        break;
//                    }
//                    else if(subjectB == subjectA || mappingOfSubjectToTeacher.get(subjectB).getLectureAllocatedValue(j, k) == 1 || subjectB.getTeachers().get(0).getLectureAllocatedValue(j+1, k) == 1){
//                        subjectB.increamentBBatchPracticalHours();
//                        System.out.println("Sondtition1 :"+(subjectB == subjectA));
//                        System.out.println("Sondtition2 :"+(mappingOfSubjectToTeacher.get(subjectB).getLectureAllocatedValue(j, k) == 1));
//                        System.out.println("Sondtition3 :"+(mappingOfSubjectToTeacher.get(subjectB).getLectureAllocatedValue(j+1, k) == 1));
//                    }
//                    else{
//                        batchSubjects.remove(--index);
//                        index++;
//                        break;                        
//                    }
//                }
//                System.out.println("subjectB is finalized : isSpecial:"+isSpecial+" subjectB is:"+subjectB.getName());
//            }
//            /*subjectB is finalized*/
//            index = 0;//restarting
//            if(noOfBatches >= 3){
//                while(true){                        
////                    subjectC = batchSubjects.get(random.nextInt(batchSubjects.size()));
//                    if(batchSubjects.size() > index){
//                        subjectC = batchSubjects.get(index++);
//                        System.out.println("subjectC is:"+subjectC.getName());
//                        while(!subjectC.decrementCBatchPracticalHours()){
//                            System.out.println("In hai bhai");
//                            if(batchSubjects.size() > index)
//                                subjectC = batchSubjects.get(index++);
//                            else{
//                                System.out.println("Indar hai");
//                                /*Just traversing the timetable*/
//    //                            for(int i=0;i<6;i++){
//    //                                for(int j1=0;j1<8;j1++){                            
//    //                                    System.out.println("Day"+i+" :"+((Day)timeTable[i]).getLecture(j1));                            
//    //                                }
//    //                            }
//                                /**
//                                 * Here we need to backtrack because the subjectC has no CBatch lectures of subject alloted!!!
//                                 * So,now the subject need to be changed!!!
//                                 */
//                                /*Checking whether it can be swapped by subjectA!!!*/
//                                Subject oldSubjectC = subjectC;
//                                Subject oldSubjectA = subjectA;
//                                boolean flagA = false;
//
//                                subjectC = subjectA;
//                                if(subjectC.decrementCBatchPracticalHours()){//This true then subjectC is compatable!!!
//                                    //Now checking for the subjectA compatablity!!!
//                                    subjectA.increamentABatchPracticalHours();
//                                    subjectA = oldSubjectC;
//                                    if(subjectA.decrementABatchPracticalHours()){
//                                        /*IF come here then it means that subjectC can be replaced by subjectA*/
//                                        System.out.println("Jam gaya A");
//                                        flagA = true;
//                                        break;
//                                    }else{
//                                        /*If here then subjectA and subjectC cannot be replaced as subjectA is not compatable!!!*/
//                                        subjectC = oldSubjectC;
//                                        subjectA = oldSubjectA;
//                                        subjectA.decrementABatchPracticalHours();
//                                        System.out.println("Nahi Jama gaya A");
//                                    }                                
//                                }else{
//                                    System.out.println("C gone by a And Ans:"+oldSubjectA.getcBatch());                                
//                                }
//                                if(!flagA){
//                                    /*subjectA cannot be replaced by the subjectC so now checking with subjectB*/
//                                    subjectC = oldSubjectC;
//                                    Subject oldSubjectB = subjectB;
//
//                                    subjectC = subjectB;
//                                    if(subjectC.decrementCBatchPracticalHours()){//This true then subjectC is compatable!!!
//                                        //Now checking for the subjectB compatablity!!!
//                                        subjectB.increamentBBatchPracticalHours();
//                                        subjectB = oldSubjectC;
//                                        if(subjectB.decrementBBatchPracticalHours()){
//                                            /*IF come here then it means that subjectC can be replaced by subjectB*/
//                                            System.out.println("Jam gaya B");
//                                            break;
//                                        }else{
//                                            /*If here then subjectB and subjectC cannot be replaced as subjectB is not compatable!!!*/
//                                            subjectC = oldSubjectC;
//                                            subjectB = oldSubjectB;
//                                            subjectB.decrementBBatchPracticalHours();
//
//                                            System.out.println("Nahi Jama gaya B");
//                                        }
//                                    }else{
//                                        System.out.println("C gone by bAns:"+oldSubjectB.getcBatch());
//                                        subjectC = oldSubjectC;
//                                        Iterator<Subject> iterator = giveSubjectsPendingByBatch('c',3).iterator();
//                                        if(iterator.hasNext())
//                                            subjectC = iterator.next();
//                                        System.out.println("Subject C changed:"+subjectC.getName());
//                                        specialFlag = true;
//                                        break;
//                                    }
//                                }                            
//                            }
//                        }
//                    }else{
//                        Iterator<Subject> iterator = giveSubjectsPendingByBatch('c',3).iterator();
//                        if(iterator.hasNext())
//                            subjectC = iterator.next();
//                        System.out.println("Subject C changed2:"+subjectC.getName());
//                        specialFlag = true;
//                        index++;
//                    }
//
//                    
////                        subjectC = batchSubjects.get(random.nextInt(batchSubjects.size()));
//
//                    if(specialFlag){
//                        /*specialFlag is true when the same lecture is there twice in batches or might not be also!!!*/
//                        System.out.println("in special");
////                        Teacher teacherObj = mappingOfSubjectToTeacher.get(subjectC);
////                        if(teacherObj == mappingOfSubjectToTeacher.get(subjectA) || teacherObj == mappingOfSubjectToTeacher.get(subjectB)){
////                            System.out.println("Undar if");
////                            /*So, now we have to take differnt teahcer*/
////                            for(Teacher teacher: subjectC.getTeachers()){
////                                if(!teacherObj.getName().equals(teacher.getName())){
////                                    if(teacher.getLectureAllocatedValue(j, k) == 0 && teacher.getLectureAllocatedValue(j+1, k) == 0){
////                                        diffCTeacher = teacher;
////                                        System.out.println("New teacher alloted");
////                                        break;
////                                    } 
////                                }
////                            }  
////                        }
//                        System.out.println("Outer loop breaked!!!");
//                        break;
//                    }else if(isSpecial){
//                        System.out.println("In");
//                        if(subjectC.getTeachers().get(2).getLectureAllocatedValue(j, k) == 1 || subjectC.getTeachers().get(2).getLectureAllocatedValue(j+1, k) == 1)
//                            subjectC.increamentCBatchPracticalHours();
//                        else{
//                            batchSubjects.remove(--index);
//                            index++;
//                            break;
//                        }
//                    }else if(subjectC == subjectA || subjectC == subjectB || mappingOfSubjectToTeacher.get(subjectC).getLectureAllocatedValue(j, k) == 1 || mappingOfSubjectToTeacher.get(subjectC).getLectureAllocatedValue(j+1, k) == 1){
//                        subjectC.increamentCBatchPracticalHours();
//                        System.out.println("Condtition1 :"+(subjectC == subjectA));
//                        System.out.println("Condtition2 :"+(subjectC == subjectB));
//                        System.out.println("Condtition3 :"+(mappingOfSubjectToTeacher.get(subjectC).getLectureAllocatedValue(j, k) == 1));
//                        System.out.println("Condtition4 :"+(mappingOfSubjectToTeacher.get(subjectC).getLectureAllocatedValue(j+1, k) == 1));
//                    }else{
//                        System.out.println("Else");
//                        batchSubjects.remove(--index);
//                        index++;
//                        break;                                
//                    }
//                }
//                System.out.println("subjectC is finalized");
//            }
//            /*subjectC is finalized*/            
//
//            /*At this point all the 3 subjects and their corresponding teachers are finalized as well as 2 lectures are free for alloting*/
//
//            /*Checking if the two consicutive lectures are free and starting lecture is even then allot it!!!*/
//            filled[j][k] = 1;
//            filled[j+1][k] = 1;
//            System.out.println("Alloted are:["+j+k+"]");
//            //Updating the teachers timetable!!!
//            subjectA.getTeachers().get(0).setLectureAllocated(j,k);//00
//            subjectA.getTeachers().get(0).setLectureAllocated(j+1,k);//10
//
//            if(noOfBatches >= 2){
//                if(diffBTeacher == null){
//                    subjectB.getTeachers().get(0).setLectureAllocated(j,k);//00
//                    subjectB.getTeachers().get(0).setLectureAllocated(j+1,k);//10                                        
//                }else{
//                    diffBTeacher.setLectureAllocated(j, k);
//                    diffBTeacher.setLectureAllocated(j+1, k);
//                }
//            }
//
//            if(noOfBatches >= 3){
//                if(diffCTeacher == null){
//                    subjectC.getTeachers().get(0).setLectureAllocated(j,k);//00
//                    subjectC.getTeachers().get(0).setLectureAllocated(j+1,k);//10                                            
//                }else{
//                    diffCTeacher.setLectureAllocated(j,k);
//                    diffCTeacher.setLectureAllocated(j+1,k);
//                }
//            }
//
//            //Updating our Timetable!!!
//            if(subjectA != null && subjectB != null && subjectC != null){
//                ArrayList<Teacher> teachers = new ArrayList<>();
//                teachers.add(mappingOfSubjectToTeacher.get(subjectA));
//                if(diffBTeacher == null)
//                    teachers.add(mappingOfSubjectToTeacher.get(subjectB));
//                else
//                    teachers.add(diffBTeacher);
//                if(diffCTeacher == null)
//                    teachers.add(mappingOfSubjectToTeacher.get(subjectC));
//                else
//                    teachers.add(diffCTeacher);
//                ((Day)timeTable[k]).addPracticalLecture(j, new PraticalLecture(subjectA.getName(), subjectB.getName(), subjectC.getName(),teachers));
//                ((Day)timeTable[k]).addPracticalLecture(j+1, new PraticalLecture(subjectA.getName(), subjectB.getName(), subjectC.getName(),teachers));                        
//            }else if(subjectA != null && subjectB != null){
//                ArrayList<Teacher> teachers = new ArrayList<>();
//                teachers.add(mappingOfSubjectToTeacher.get(subjectA));
//                teachers.add(mappingOfSubjectToTeacher.get(subjectB));
//                ((Day)timeTable[k]).addPracticalLecture(j, new PraticalLecture(subjectA.getName(), subjectB.getName(),teachers));
//                ((Day)timeTable[k]).addPracticalLecture(j+1, new PraticalLecture(subjectA.getName(), subjectB.getName(),teachers));                        
//            }else{
//                ((Day)timeTable[k]).addPracticalLecture(j, new PraticalLecture(subjectA.getName(),mappingOfSubjectToTeacher.get(subjectA)));
//                ((Day)timeTable[k]).addPracticalLecture(j+1, new PraticalLecture(subjectA.getName(),mappingOfSubjectToTeacher.get(subjectA)));                        
//            }
//
//            flag = true; //For breaking the outer loop!!!
//            return true;
//        }//end of if!!!
//
//        return false;
//    }
    
    private boolean allotPraticalLectures(ArrayList<Subject> batchSubjects,Map<Subject,Teacher> mappingOfSubjectToTeacher,int j,int k){
        int index = 0;
        Random random = new Random();
        Teacher subjectATeacher,subjectBTeacher = null,subjectCTeacher = null;
        Collections.shuffle(batchSubjects);        
        /*Checking that lecture is not already alloted!!!*/
        /*Checking if the two consicutive lectures are free!!!*/
        if(filled[j][k] == 0 && filled[j+1][k] == 0){
            /*Now, we need to choose 3 random subjects who has 3 pratical batches*/
            Subject subjectA,subjectB,subjectC;
            subjectA = subjectB = subjectC = null;
            int noOfBatches = batchSubjects.get(0).getNoOfBatches();
            while(true){
                System.out.println("Loop hai bnhai: And index:"+index);
//                subjectA = batchSubjects.get(random.nextInt(batchSubjects.size()));
                subjectA = batchSubjects.get(index++); // error line
                System.out.println("SubjectA Name:"+subjectA.getName());
                System.out.println("Answerrrrrrrrrr is :"+subjectA.getaBatch());
                System.out.println("Batch is :"+batchSubjects);
                System.out.println("Index is :"+index);
                while(!subjectA.decrementABatchPracticalHours()){
                    System.out.println("Index:"+index);
                    subjectA = batchSubjects.get(index);
                    System.out.println("Final Answerrrrrrrrrrrrrr:"+subjectA.getaBatch());
                    System.out.println("Final Answerrrrrrrrrrrrrr123:"+subjectA.getName());
                    index++;
                }
                subjectATeacher = mappingOfSubjectToTeacher.get(subjectA);
                
                if(subjectATeacher.getLectureAllocatedValue(j, k) == 1 || subjectATeacher.getLectureAllocatedValue(j+1, k) == 1){
                    System.out.println("Condtion1:"+subjectATeacher.getLectureAllocatedValue(j, k));
                    System.out.println("Condtion1:"+subjectATeacher.getLectureAllocatedValue(j+1, k));
                    subjectA.increamentABatchPracticalHours();
                    System.out.println("In subjectA condtion and j:"+j+"and k:"+k);
                }else{
                    --index;
                    System.out.println("BatchSub before is:"+batchSubjects);
                    batchSubjects.remove(index);
                    System.out.println("Index which is removed:"+index);
                    System.out.println("BatchSubjects is:"+batchSubjects);
                    System.out.println("The main is subjectName:"+subjectA.getName()+" And subjectATeacher:"+subjectATeacher.getName());
                    break;
                }            
            }//end of while
            System.out.println("subjectA is finalized: subjectA is:"+subjectA.getName());
            index = 0;//restarting
            /*subjectA is finalized*/

//            boolean subjectBFinalized = false;
            
            if(noOfBatches >= 2){
                while(true){
                    subjectB = batchSubjects.get(index++);
                    while(!subjectB.decrementBBatchPracticalHours()){
                        /*Re-select the subjectB now!!!*/

                        /*When here comes then subjectB is not proper and need to re-select subjectB!!!*/
                        if(index >= batchSubjects.size())
                        {
                            /*Now you have to change subjectA*/
                            Iterator<Subject> iterator = giveSubjectsPendingByBatch('b',noOfBatches,j,k).iterator();
                            boolean subjectAChanged = false;
                            while(iterator.hasNext()){
                                subjectB = iterator.next();
                                if(subjectA.equals(subjectB)){
                                    /*Now we have to check that subjectA can be changed and if not then subjectB needs to change in next iteration!!!*/
                                    iterator = giveSubjectsPendingByBatch('a',noOfBatches,j,k).iterator();
                                    while(iterator.hasNext()){                                    
                                        subjectA = iterator.next();
                                        if((!subjectA.equals(subjectB)) && mappingOfSubjectToTeacher.get(subjectA).getLectureAllocatedValue(j, k) == 0 && mappingOfSubjectToTeacher.get(subjectA).getLectureAllocatedValue(j+1, k) == 0){
                                            subjectAChanged = true;
                                            break;
                                        }
                                    }
                                    if(subjectAChanged)                                        
                                        break;
                                }else
                                    break;
                            }
                            System.out.println("Subject B changed1:"+subjectB.getName());
                            System.out.println("Subject A name is:"+subjectA.getName());
                        }else{                                                        
                            subjectB = batchSubjects.get(index++);                            
                        }
                    }//inner while
//                    if(subjectBFinalized)
//                        break;
                    subjectBTeacher = mappingOfSubjectToTeacher.get(subjectB);
                    if(subjectA.equals(subjectB) || subjectBTeacher.getLectureAllocatedValue(j, k) == 1 || subjectBTeacher.getLectureAllocatedValue(j+1, k) == 1)
                        subjectB.increamentBBatchPracticalHours();
                    else{
                        System.out.println("The main is subjectName:"+subjectB.getName()+" And subjectBTeacher:"+subjectBTeacher.getName());

                        /*Here the subjectB is proper and therefore selected!!!*/
                        batchSubjects.remove(subjectB);
//                        subjectBFinalized = true;
                        break;
                    }
                }//outer while
            }
//            CPU :- Processor karvata hai Register left and right shift
            System.out.println("subjectB is finalized: subjectA is:"+subjectA.getName());
            System.out.println("subjectB is finalized: subjectB is:"+subjectB.getName());

            /*subjectB is finalized*/
            index = 0;//restarting

            if(noOfBatches >= 3){
                while(true){
                    System.out.println("SubjectA: ka aBatch:"+subjectA.getaBatch()+" bBatch:"+subjectA.getbBatch()+" cBatch:"+subjectA.getcBatch());
                    System.out.println("SubjectB: ka aBatch:"+subjectB.getaBatch()+" bBatch:"+subjectB.getbBatch()+" cBatch:"+subjectB.getcBatch());
                    System.out.println("Batch subjects inside c are:"+batchSubjects);
                    subjectC = batchSubjects.get(index++);                    
                    while(!subjectC.decrementCBatchPracticalHours()){
                        /*Re-select the subjectC now!!!*/

                        /*When here comes then subjectC is not proper and need to re-select subjectC!!!*/
                        if(index >= batchSubjects.size())
                        {
                            /*Now you have to change subjectA*/
                            Iterator<Subject> iterator = giveSubjectsPendingByBatch('c',noOfBatches,j,k).iterator();
                            boolean subjectChanged = false;
                            while(iterator.hasNext()){
                                subjectC = iterator.next();
                                if(subjectC.equals(subjectA)){
                                    /*Now we have to check that subjectA or subjectB can be changed and if not then subjectC needs to change in next iteration!!!*/
                                    
                                    iterator = giveSubjectsPendingByBatch('a',noOfBatches,j,k).iterator();
                                    while(iterator.hasNext()){      
                                        subjectA = iterator.next();
                                        if((!subjectA.equals(subjectC)) && mappingOfSubjectToTeacher.get(subjectA).getLectureAllocatedValue(j, k) == 0 && mappingOfSubjectToTeacher.get(subjectA).getLectureAllocatedValue(j+1, k) == 0){
                                            subjectChanged = true;
                                            break;
                                        }
                                    }
                                    if(subjectChanged)                                        
                                        break;
                                }else if(subjectC.equals(subjectB)){
                                    /*Now we have to check that subjectA or subjectB can be changed and if not then subjectC needs to change in next iteration!!!*/
                                    
                                    iterator = giveSubjectsPendingByBatch('b',noOfBatches,j,k).iterator();
                                    while(iterator.hasNext()){                                    
                                        subjectB = iterator.next();
                                        if((!subjectB.equals(subjectC)) && mappingOfSubjectToTeacher.get(subjectB).getLectureAllocatedValue(j, k) == 0 && mappingOfSubjectToTeacher.get(subjectB).getLectureAllocatedValue(j+1, k) == 0){
                                            subjectChanged = true;
                                            break;
                                        }
                                    }
                                    if(subjectChanged)                                        
                                        break;
                                }else
                                    break;
                            }
//                            System.out.println("Subject B changed1:"+subjectB.getName());
                        }else{
                            subjectC = batchSubjects.get(index++);
                        }                                                
                    }//inner while
                    subjectCTeacher = mappingOfSubjectToTeacher.get(subjectC);
                    if(subjectC.equals(subjectA) || subjectC.equals(subjectB) || subjectCTeacher.getLectureAllocatedValue(j, k) == 1 || subjectCTeacher.getLectureAllocatedValue(j+1, k) == 1)
                        subjectC.increamentCBatchPracticalHours();
                    else{
                        batchSubjects.remove(--index);
                        break;
                    }
                }//outer while
            }
            
            /*subjectC is finalized*/
            /*At this point all the subjects and their corresponding teachers are finalized*/

            filled[j][k] = 1;
            filled[j+1][k] = 1;
            System.out.println("Alloted are:["+j+k+"]");
            //Updating the teachers timetable!!!
            
            subjectATeacher.setLectureAllocated(j, k);
            subjectATeacher.setLectureAllocated(j+1, k);

            if(noOfBatches >= 2){
                subjectBTeacher.setLectureAllocated(j, k);
                subjectBTeacher.setLectureAllocated(j+1, k);
            }

            if(noOfBatches >= 3){
                subjectCTeacher.setLectureAllocated(j, k);
                subjectCTeacher.setLectureAllocated(j+1, k);
            }

            ArrayList<Teacher> teachersList;
            //Updating our Timetable!!!
            switch(subjectA.getNoOfBatches()){
                case 1: ((Day)timeTable[k]).addPracticalLecture(j, new PraticalLecture(subjectA.getName(),subjectATeacher));
                        ((Day)timeTable[k]).addPracticalLecture(j+1, new PraticalLecture(subjectA.getName(),subjectATeacher));
                        break;
                case 2: teachersList = new ArrayList<>();
                        teachersList.add(subjectATeacher);
                        teachersList.add(subjectBTeacher);
                        ((Day)timeTable[k]).addPracticalLecture(j, new PraticalLecture(subjectA.getName(), subjectB.getName(),teachersList));
                        ((Day)timeTable[k]).addPracticalLecture(j+1, new PraticalLecture(subjectA.getName(), subjectB.getName(),teachersList));                        

                        break;
                case 3: teachersList = new ArrayList<>();
                        teachersList.add(subjectATeacher);
                        teachersList.add(subjectBTeacher);
                        teachersList.add(subjectCTeacher);
                        ((Day)timeTable[k]).addPracticalLecture(j, new PraticalLecture(subjectA.getName(), subjectB.getName(),subjectC.getName(),teachersList));
                        ((Day)timeTable[k]).addPracticalLecture(j+1, new PraticalLecture(subjectA.getName(), subjectB.getName(),subjectC.getName(),teachersList));                        
                        break;
            }
            return true;
        }
        return false;
    }
    
    private boolean checkSpecialPraticalLecturesCondition(Subject subjectObj,int j,int k){
        int numBatches = subjectObj.getNoOfBatches();
//        switch(numBatches){
//            case 1: 
//                    
//                    break;
//            case 2: if(subjectObj.getTeachers().get(0).getLectureAllocatedValue(j, k) == 0 && subjectObj.getTeachers().get(1).getLectureAllocatedValue(j, k) == 0)
//                        return true;
//                    break;
//            case 3: if(subjectObj.getTeachers().get(0).getLectureAllocatedValue(j, k) == 0 && subjectObj.getTeachers().get(1).getLectureAllocatedValue(j, k) == 0 && subjectObj.getTeachers().get(2).getLectureAllocatedValue(j, k) == 0)
//                        return true;
//                    break;
//        }
        for(int i=0;i<numBatches;i++){
            if(subjectObj.getTeachers().get(i).getLectureAllocatedValue(j, k) == 1 && subjectObj.getTeachers().get(i).getLectureAllocatedValue(j+1, k) == 1)
                return false;
        }
        return true;
    }
    
    private void allotSpecialPraticalLectures(Subject subjectObj,int j,int k){
        String subjectName = subjectObj.getName();//Which is common for all the subjects either 1,2 or 3!!!
        int numBatches = subjectObj.getNoOfBatches();

        switch(numBatches){
            case 1: ((Day)timeTable[k]).addPracticalLecture(j, new PraticalLecture(subjectName,subjectObj.getTeachers().get(0)));
                    ((Day)timeTable[k]).addPracticalLecture(j+1, new PraticalLecture(subjectName,subjectObj.getTeachers().get(0)));
                    break;
            case 2: ((Day)timeTable[k]).addPracticalLecture(j, new PraticalLecture(subjectName, subjectName,subjectObj.getTeachers()));
                    ((Day)timeTable[k]).addPracticalLecture(j+1, new PraticalLecture(subjectName, subjectName,subjectObj.getTeachers()));                        

                    break;
            case 3: ((Day)timeTable[k]).addPracticalLecture(j, new PraticalLecture(subjectName, subjectName, subjectName,subjectObj.getTeachers()));
                    ((Day)timeTable[k]).addPracticalLecture(j+1, new PraticalLecture(subjectName, subjectName, subjectName,subjectObj.getTeachers()));                        
                    break;
        }
        
        for(int i=0;i<numBatches;i++){
            subjectObj.getTeachers().get(i).setLectureAllocated(j,k);//00
            subjectObj.getTeachers().get(i).setLectureAllocated(j+1,k);//10 
        }
    }
    
    public static void main(String[] args) {
        ArrayList<Teacher> temp1 = new ArrayList<>();
        temp1.add(new Teacher("Sonali Pawar"));
        temp1.add(new Teacher("Aarati Mahajan"));
        temp1.add(new Teacher("Mahadeo Narlawar"));
        
        ArrayList<Subject> temp = new ArrayList<>();
//        temp.add(new Subject("Physics", temp1, 6, 5, 3));
//        temp.add(new Subject("Chemistry", temp1, 6, 6, 3));
//        temp.add(new Subject("WPC", temp1, 8, 8, 3));
//        temp.add(new Subject("Physics", temp1, 6, 0, 3));
//        temp.add(new Subject("Chemistry", temp1, 6, 6, 3));
//        temp.add(new Subject("WPC", temp1, 8, 0, 3));
//        new MainTimetable(1, 4, 1, temp);
    }
}