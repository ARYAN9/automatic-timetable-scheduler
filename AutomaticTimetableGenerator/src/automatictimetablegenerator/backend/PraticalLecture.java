/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.backend;

import java.util.ArrayList;

/**
 *
 * @author Aryan Dhami
 */
public class PraticalLecture implements Lecture{
    private final int lecture_type;
    private String aBatchName,bBatchName,cBatchName;
    private ArrayList<Teacher> teachers;//For two and three batch subjects!!!
//    Teacher teacherObj = null;//Only for the one batch subjects!!!
    
//    public PraticalLecture() {
//        this("","","");
//    }

    public PraticalLecture(String aBatchName,Teacher teacherObj) {
        this.lecture_type = LectureConstants.PRACTICALS;
        this.aBatchName = aBatchName;
        this.bBatchName = "";
        this.cBatchName = "";
        this.teachers.add(teacherObj);
    }

    public PraticalLecture(String aBatchName, String bBatchName,ArrayList<Teacher> teachers) {
        this.lecture_type = LectureConstants.PRACTICALS;
        this.aBatchName = aBatchName;
        this.bBatchName = bBatchName;
        this.cBatchName = "";
        this.teachers = teachers;
    }

    public PraticalLecture(String aBatchName, String bBatchName, String cBatchName,ArrayList<Teacher> teachers) {
        this.lecture_type = LectureConstants.PRACTICALS;
        this.aBatchName = aBatchName;
        this.bBatchName = bBatchName;
        this.cBatchName = cBatchName;
        this.teachers = teachers;
    }
    
    @Override
    public int getLectureType(){
        return this.lecture_type;
    }
    
    @Override
    public String toString(){
        if("".equals(bBatchName) && "".equals(cBatchName))
                return "Pratical Lecture : aBatch:"+this.aBatchName
                        +"\n teahcers are: 1:"+teachers.get(0);
        else if("".equals(cBatchName))
                return "Pratical Lecture : aBatch:"+this.aBatchName+" and bBatch:"+this.bBatchName
                    +"\n teahcers are: 1:"+teachers.get(0)+" 2:"+teachers.get(1);
   
        return "Pratical Lecture : aBatch:"+this.aBatchName+" and bBatch:"+this.bBatchName+" and cBatch:"+this.cBatchName
                +"\n teahcers are: 1:"+teachers.get(0)+" 2:"+teachers.get(1)+" 3:"+teachers.get(2);
    }
//  public PraticalLecture()
    /**
     * SOME GETTERS:-
     */
    public String getABatchName() {
        return aBatchName;
    }

    public String getBBatchName() {
        return bBatchName;
    }

    public String getCBatchName() {
        return cBatchName;
    }

    public ArrayList<Teacher> getTeachers(){
//        return new ArrayList<>(teachers);
        return teachers;
    }
    
    public void setTeacher(Teacher teacherObj,int index){
        this.teachers.set(index, teacherObj);
        System.out.println("Teacher seeted :"+teacherObj.getName()+" ANd index is:"+index);
    }
}
