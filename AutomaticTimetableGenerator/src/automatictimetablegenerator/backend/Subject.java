/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.backend;

import java.util.ArrayList;

/**
 *
 * @author Aryan Dhami
 */
public class Subject implements Comparable<Subject>{
    private String name;
    private int praticalHours,theoryHours,noOfBatches,aBatch,bBatch,cBatch;
    //    private Teacher teacherObj;
    private ArrayList<Teacher> teachers;
    private ArrayList<Integer> practicalLectureRowList,practicalLectureColumnList,theoryLectureRowList,theoryLectureColumnList;
    
    public Subject(String name, ArrayList<Teacher> teachers, int praticalHours,int theoryHours,int noOfBatches,ArrayList<Integer> practicalLectureRowList,ArrayList<Integer> practicalLectureColumnList,ArrayList<Integer> theoryLectureRowList,ArrayList<Integer> theoryLectureColumnList) {
        this.name = name;
        this.teachers = teachers;
        this.praticalHours = praticalHours;
        this.theoryHours = theoryHours;
        this.noOfBatches = noOfBatches;
        this.practicalLectureRowList = practicalLectureRowList;
        this.practicalLectureColumnList = practicalLectureColumnList;
        this.theoryLectureRowList = theoryLectureRowList;
        this.theoryLectureColumnList = theoryLectureColumnList;
        this.aBatch = this.bBatch = this.cBatch = this.praticalHours;
        System.out.println("aBatch value oin constructir:"+aBatch);
    }
    
//    public boolean decrementPracticalHours(){
//        if(this.praticalHours <= 0){
//            return false;
//        }
//        this.praticalHours = this.praticalHours - 2;
//        System.out.println("Currently:"+this.praticalHours);
//        return true;
//    }
//    
    public boolean decrementTheoryHours(){
        if(this.theoryHours <= 0){
            return false;
        }
        this.theoryHours = this.theoryHours - 1;
        return true;
    }
    
    public boolean decrementABatchPracticalHours(){
        if(this.aBatch <= 0)
            return false;        
        this.aBatch = this.aBatch - 2;
//        System.out.println("aBatch Value:"+aBatch);
        return true;
    }

    public boolean decrementBBatchPracticalHours(){
        if(this.bBatch <= 0)
            return false;        
        this.bBatch = this.bBatch - 2;
        System.out.println("bBatch Value:"+bBatch);
        return true;
    }

    public boolean decrementCBatchPracticalHours(){
        if(this.cBatch <= 0)
            return false;        
        this.cBatch = this.cBatch - 2;
        System.out.println("cBatch Value:"+cBatch);
        return true;
    }
    
    public void increamentABatchPracticalHours(){
        this.aBatch = this.aBatch + 2;
    }

    public void increamentBBatchPracticalHours(){
        this.bBatch = this.bBatch + 2;
        System.out.println("bBatch Value Incremeneted:"+bBatch);
    }
    
    public void increamentCBatchPracticalHours(){
        this.cBatch = this.cBatch + 2;
        System.out.println("cBatch Value Incremented:"+cBatch);
    }

    /**
     * GETTERS AND SETTERS
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public int getPraticalHours() {
        return praticalHours;
    }

    public void setPraticalHours(int praticalHours) {
        this.praticalHours = praticalHours;
    }

    public int getTheoryHours() {
        return theoryHours;
    }

    public void setTheoryHours(int theoryHours) {
        this.theoryHours = theoryHours;
    }
    
    public int getNoOfBatches() {
        return noOfBatches;
    }

    public void setNoOfBatches(int noOfBatches) {
        this.noOfBatches = noOfBatches;
    }
    
    public ArrayList<Teacher> getTeachers() {
//        return new ArrayList(this.teachers);//So, this is not modifiable!!!
        return this.teachers;
    }
    
    public int getaBatch() {
        return aBatch;
    }

    public void setaBatch(int aBatch) {
        this.aBatch = aBatch;
    }

    public int getbBatch() {
        return bBatch;
    }

    public void setbBatch(int bBatch) {
        this.bBatch = bBatch;
    }

    public int getcBatch() {
        return cBatch;
    }

    public void setcBatch(int cBatch) {
        this.cBatch = cBatch;
    }

    public ArrayList<Integer> getPracticalLectureRowList(){
        return new ArrayList<>(this.practicalLectureRowList);
    }
    
    public ArrayList<Integer> getPracticalLectureColumnList(){
        return new ArrayList<>(this.practicalLectureColumnList);
    }

    public ArrayList<Integer> getTheoryLectureRowList(){
        return new ArrayList<>(this.theoryLectureRowList);
    }
    
    public ArrayList<Integer> getTheoryLectureColumnList(){
        return new ArrayList<>(this.theoryLectureColumnList);
    }
    
//    public void setTeacherObj(Teacher teacherObj) {
//        this.teacherObj = teacherObj;
//    }
    @Override
    public String toString(){
        return this.name;
    }
    
    @Override
    public int compareTo(Subject subjectObj) {
        return this.name.compareTo(subjectObj.name);
    }
    
    @Override
    public int hashCode() {        
//        return Objects.hashCode(this.name);
        return this.name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Subject)
            return this.name.equals(((Subject)obj).getName());
        return false;
    }

}
