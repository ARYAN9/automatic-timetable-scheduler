/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.backend;

import java.util.Objects;

/**
 *
 * @author Aryan Dhami
 */
public class Teacher implements Comparable<Teacher>{

    private String name;
    private int lectureAllocated[][];
    private int totalLecturesRemaining = 48;
    
    public Teacher(String name) {   
//        this.lectureAllocated = new int[6][6];
        this.lectureAllocated = new int[8][6];
        this.name = name;
    }
    
    public int getLectureAllocatedValue(int row,int column){
        return this.lectureAllocated[row][column];
    }

    public void setLectureAllocated(int row,int column){
        System.out.println("Alloted hua haui");
        System.out.println("ROw:"+row+"Column:"+column);
        this.lectureAllocated[row][column] = 1;
        this.totalLecturesRemaining--;
    }
    
    public int getTotalLecturesRemaining() {
        return totalLecturesRemaining;
    }
    
    public int[][] getLectureAllocated() {
//        return lectureAllocated.clone();
        int lectureAllocatedCopy[][] = new int[8][6];
        for(int i=0;i<8;i++){
            for(int j=0;j<6;j++){
                lectureAllocatedCopy[i][j] = lectureAllocated[i][j];
            }
        }
        return lectureAllocatedCopy;
    }    
    
    public String getName(){ return this.name; }
    
    @Override
    public String toString(){
        return this.name;
    }
    
    @Override
    public int compareTo(Teacher teacherObj) {
        return this.name.compareTo(teacherObj.name);
    }
    
    @Override
    public int hashCode() {        
//        return Objects.hashCode(this.name);
        return this.name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Teacher)
            return this.name.equals(((Teacher)obj).getName());
        return false;
    }
}
