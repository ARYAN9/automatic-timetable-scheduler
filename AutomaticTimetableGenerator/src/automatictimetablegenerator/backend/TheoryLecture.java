/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.backend;

/**
 *
 * @author Aryan Dhami
 */
public class TheoryLecture implements Lecture{
    private final int lecture_type;
    private String name;
    private Teacher teacherObj;

    public TheoryLecture(String name,Teacher teacherObj) {
        this.lecture_type = LectureConstants.LECTURE;
        this.name = name;
        this.teacherObj = teacherObj;
    }

    @Override
    public int getLectureType(){
        return this.lecture_type;
    }
    
    @Override
    public String toString(){
        return "Theory Lecture : name:"+this.name;
    }
    
    /**
     * Getter
     */
    public String getName() {
        return name;
    }
    
    public Teacher getTeacher() {
        return teacherObj;
    }
}
