/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 *
 * @author DELL
 */
public class Tp {
    public String readFile(){
        String str,outputString = "";
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File("./src/automatictimetablegenerator/CO1.xml")))))
        {
            while((str=br.readLine())!=null)
            {
                    outputString = outputString + str;
            }
        }
        catch (IOException ie){
            System.out.println("Error while reading the file:"+ie.getMessage());
            return null;
        }
        return outputString;//When the exception comes the "" blank string will be returned!!!
    }
    public Day[] parseTimeTable(){
        Day[] timeTable = new Day[8];//6+2extra lec
        /**
         * So every timeTable has 6 days and every 6 days has 6+2 lectures!!!
         */
        for(int i=0;i<8;i++){
            timeTable[i] = new Day();
        }
        
        String xmlFileContents = readFile(),content = "";
        System.out.println("Content :"+xmlFileContents);
        if(xmlFileContents == null){
            return null;
        }
//        String regex = "<day>((\\s.*?)*)<\\/day>";//Regex for the each day contents!!!
        String regex = "<day>(\\s.*?)<\\/day>";//Regex for the each day contents!!!
        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);//MULTILINE because it goes till end of the entire input sequence and not just till \n which is default!!!
        final Matcher matcher = pattern.matcher(xmlFileContents);
        while (matcher.find()) {
            content = matcher.group(1);//Will contain the each day part!!!
            System.out.println("Content is: "+content);
            regex = "<dayNo>(.*?)<\\/dayNo>";//Regex for each day number!!!
            Pattern dayNoPattern = Pattern.compile(regex, Pattern.MULTILINE);
            Matcher dayNoMatcher = dayNoPattern.matcher(content);
            //dayNoMatcher will always contain one because there will always be only one <dayNo> tag!!!
            int dayNo = -1, lectureNo = -1;
            if(dayNoMatcher.find())
                dayNo = Integer.parseInt(dayNoMatcher.group(1));
            System.out.println("DayNo is:"+dayNo);
            regex = "<lecture>\\s*<lectureNo>(.*?)<\\/lectureNo>\\s*(<thoery>|<practical>)\\s*((\\s*?.*?)*)\\s*(<\\/thoery>|<\\/practical>)\\s*<\\/lecture>";
            pattern = Pattern.compile(regex, Pattern.MULTILINE);
            Matcher lectureMatcher = pattern.matcher(content);
            while(lectureMatcher.find()){
                lectureNo = Integer.parseInt(lectureMatcher.group(1));
                System.out.println("Lecture No is: "+lectureNo);
                if("<practical>".equals(lectureMatcher.group(2))){
                    //It is practical Lecture!!!
//                    content = ;
                    System.out.println("Bhai ye dekh: "+lectureMatcher.group(3));
                    regex = "(<a>|<b>|<c>)((.*?\\s*?)*)<teacher>((.*?\\s*?)*)</teacher>((.*?\\s*?)*)(</a>|</b>|</c>)";//1,2,4
                    pattern = Pattern.compile(regex, Pattern.MULTILINE);
                    Matcher practicalMatcher = pattern.matcher(lectureMatcher.group(3));
                    ArrayList<Teacher> allBatchTeachers = new ArrayList<>();
                    ArrayList<String> allBatchSubjects = new ArrayList<>();
                    int size =0;
                    while(practicalMatcher.find()){//Only one!!!
                        
                        String batchIndicator = practicalMatcher.group(1);
//                        String subjectName = practicalMatcher.group(2).replaceAll("\\s", "");
//                        String teacherName = practicalMatcher.group(4).replaceAll("\\s", "");
                        String subjectName = practicalMatcher.group(2).trim();
                        System.out.println("Subject Name is: "+subjectName);
                        String teacherName = practicalMatcher.group(4).trim();
//                        System.out.println("TEacher NAme:"+teacherName);
//                        teachers.get(teacherName).setLectureAllocated(dayNo, lectureNo);
//                        if("<a>".equals(batchIndicator))
//                        allBatchTeachers.add(teachers.get(teacherName));
                        allBatchSubjects.add(subjectName);
//                        else if("<b>".equals(batchIndicator)){
//                            allBatchTeachers.add(teachers.get(teacherName));
//                            allBatchSubjects.add(subjectName);
//                        }
//                        else if("<c>".equals(batchIndicator))
                        
//                            ; 
                        size++;
                    }
//                    int size = allBatchTeachers.size();
                    switch (size) {
                        case 1:
                            ((Day)timeTable[dayNo]).addPracticalLecture(lectureNo, new PraticalLecture(allBatchSubjects.get(0), allBatchTeachers.get(0)));
                            break;
                        case 2:
                            ((Day)timeTable[dayNo]).addPracticalLecture(lectureNo, new PraticalLecture(allBatchSubjects.get(0),allBatchSubjects.get(1), allBatchTeachers));
                            break;
                        case 3:
                            ((Day)timeTable[dayNo]).addPracticalLecture(lectureNo, new PraticalLecture(allBatchSubjects.get(0),allBatchSubjects.get(1),allBatchSubjects.get(2), allBatchTeachers));
                            break;
                    }
                }else{
                    //It is theory Lecture!!!
                    regex = "(.*?)\\s*<teacher>\\s*(.*?)\\s*</teacher>";//Group 1 and 2!!!
                    pattern = Pattern.compile(regex, Pattern.MULTILINE);
                    Matcher theoryMatcher = pattern.matcher(lectureMatcher.group(3));
                    if(theoryMatcher.find()){//Only one!!!
//                        String subjectName = theoryMatcher.group(1);
//                        String teacherName = theoryMatcher.group(2);
//                        Teacher teacherObj = teachers.get(theoryMatcher.group(2));
//                        teacherObj.setLectureAllocated(dayNo, lectureNo);
                        ((Day)timeTable[dayNo]).addThoeryLecture(lectureNo, new TheoryLecture(theoryMatcher.group(1), new Teacher(theoryMatcher.group(2))));
                    }
                }
//                lectureMatcher.group(4);//This is helper group!!!
            }
                    
//            System.out.println("Full match: " + matcher.group(0));
//            for (int i = 1; i <= matcher.groupCount(); i++) {
//                System.out.println("Group " + i + ": " + matcher.group(i));
//            }
//            content = matcher.group(2);
//            final Pattern pattern1;
//            final Matcher matcher1;
//            if(matcher.group(1).equals("<theory>")){
//                regex = "(.*?)\\s*<teacher>\\s*(.*?)\\s*</teacher>";//Group 1 and 2!!!
//                pattern1 = Pattern.compile(regex, Pattern.MULTILINE);
//                
//            }else if(matcher.group(1).equals("<practical>")){
//                
//            }
        }
        System.out.println("Rewturn kiya bhau!!");
        return timeTable;
    }
    public static void main(String[] args) {
//        Day[] days = new Tp().parseTimeTable();
//        for(int i=0;i<6;i++){
//            for(int j=0;j<8;j++){     
//                System.out.println("Day"+i+" :"+((Day)((days)[i])).getLecture(j));
//            }                                            
//        }
        
//        System.out.println("Idar dekh:");
//        IntStream.iterate(1,  i -> i + 2).limit(3).forEach(i ->{
//            System.out.println("i:"+i);
//        });
        HashMap<String,CollisionAvoidanceHelper> tp = new HashMap<>();
        HashMap<String,CollisionAvoidanceHelper> tp1 = new HashMap<>();
        tp.put("Jalsa",new CollisionAvoidanceHelper(new Teacher("Aryan")));
        tp.put("Jalsa1",new CollisionAvoidanceHelper(new Teacher("Anita")));
//        tp.put("Jalsa1","Anita");
//        tp1 = tp;
//        tp1 = (HashMap<String, CollisionAvoidanceHelper>) tp.clone();
        tp1 = CollisionAvoidanceHelper.deepCopy(tp);
        

        tp.get("Jalsa").getTeacherObj().setLectureAllocated(2, 2);
        System.out.println("Answer1:"+tp.get("Jalsa").getTeacherObj().getTotalLecturesRemaining());
        System.out.println("Answer2:"+tp1.get("Jalsa").getTeacherObj().getTotalLecturesRemaining());

        tp.get("Jalsa").setLectureAlloted(2, 2);
        System.out.println("Answer1:"+tp.get("Jalsa").getTotalFreeLecturesCount());
        System.out.println("Answer2:"+tp1.get("Jalsa").getTotalFreeLecturesCount());

        
        tp.put("JalsaHai", new CollisionAvoidanceHelper(new Teacher("JitenBhai")));
        System.out.println("Answer3:"+tp.get("JalsaHai"));
        System.out.println("Answer4:"+tp1.get("JalsaHai"));
        
//        getTeacherObj();
        
//        tp1.put("Wow","Jiten");
//        System.out.println("Data:"+tp1.get("Wow"));
//        System.out.println("Data:"+tp.get("Wow"));
//        
//        tp.put("Wow123","Jiten123");
//        System.out.println("Data:"+tp1.get("Wow123"));
//        System.out.println("Data:"+tp.get("Wow123"));
//        
//        tp.put("Jalsa1","New");
//        System.out.println("Data:"+tp1.get("Jalsa1"));//Anita
//        System.out.println("Data:"+tp.get("Jalsa1"));//New
        
    //        Data:Jiten
    //Data:null
    //Data:null
    //Data:Jiten123
    }
}
