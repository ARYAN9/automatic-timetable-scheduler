package automatictimetablegenerator.backend;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Aryan Dhami
 */
public class XMLParser {
    private String filePath;
//    private Day timeTable[];
    
    public XMLParser(String filePath) {
        this.filePath = filePath;
//        this.timeTable = timeTable;
    }
    
    public void writeTimetable(Day timeTable[]){
        String xmlString = "<timetable>\r\n\t";
        Lecture lectureObj = null;
        ArrayList<Teacher> teachers;
//        Teacher teacherObj;
        for(int i=0;i<6;i++){
            xmlString = xmlString + "<day>\r\n\t\t<dayNo>"+i+"</dayNo>\r\n\t\t";
            for(int j=0;j<8;j++){
                if(timeTable[i] instanceof Day)
                    lectureObj = ((Day)timeTable[i]).getLecture(j);
                if(lectureObj != null){
                    xmlString = xmlString + "<lecture>\r\n\t\t\t<lectureNo>"+j+"</lectureNo>";
                    if(lectureObj.getLectureType() == LectureConstants.LECTURE){
//                        teacherObj = ;
                        xmlString = xmlString + "\r\n\t\t\t<thoery>\r\n"+((TheoryLecture)lectureObj).getName()
                                    +"\t\t\t\t<teacher>\r\n\t\t\t\t\t"+((TheoryLecture)lectureObj).getTeacher().getName()
                                    +"\r\n\t\t\t\t</teacher>\r\n\t\t\t\t</thoery>\r\n\t\t\t\t</lecture>";
                        
                    }else if(lectureObj.getLectureType() == LectureConstants.PRACTICALS){
                        teachers = ((PraticalLecture)lectureObj).getTeachers();
                        int size = teachers.size();
                        if(size == 3){
                            xmlString = xmlString + "\r\n\t\t\t<practical>\r\n\t\t\t\t<a>\r\n\t\t\t\t\t"
                                        +((PraticalLecture)lectureObj).getABatchName()
                                        + "\r\n\t\t\t\t\t<teacher>\r\n\t\t\t\t\t\t"+ teachers.get(0) + "\r\n\t\t\t\t\t</teacher>\r\n\t\t\t\t</a>\r\n\t\t\t\t<b>\r\n\t\t\t\t\t" 
                                        +((PraticalLecture)lectureObj).getBBatchName()
                                        + "\r\n\t\t\t\t\t<teacher>\r\n\t\t\t\t\t\t"+ teachers.get(1) + "\r\n\t\t\t\t\t</teacher>\r\n\t\t\t\t</b>\r\n\t\t\t\t<c>\r\n\t\t\t\t\t" 
                                        + ((PraticalLecture)lectureObj).getABatchName()
                                        + "\r\n\t\t\t\t\t<teacher>\r\n\t\t\t\t\t\t"+ teachers.get(0) + "\r\n\t\t\t\t\t</teacher>\r\n\t\t\t\t</c>\r\n\t\t\t</practical>\r\n\t\t</lecture>\r\n\t";
                        }else if(size == 2){
                            xmlString = xmlString + "<practical>"+"<a>"+((PraticalLecture)lectureObj).getABatchName()
                                        + "<teacher>"+ teachers.get(0) + "</teacher></a><b>" +((PraticalLecture)lectureObj).getBBatchName()
                                        + "<teacher>"+ teachers.get(1) + "</teacher></b></practical></lecture>";
                        }else{
                            xmlString = xmlString + "<practical>"+"<a>"+((PraticalLecture)lectureObj).getABatchName()
                                        + "<teacher>"+ teachers.get(0) + "</teacher></a></practical></lecture>";
                        }
//                        xmlString = xmlString + "<thoery>"+((TheoryLecture)lectureObj).getName()
                    }                    
                }

                System.out.println("Day"+i+" :"+((Day)timeTable[i]).getLecture(j));
//                System.out.println("Day"+i+" :"+((Day)timeTable[i]).getLecture(j));                
            }
            xmlString = xmlString + "</day>\r\n";
        }
        xmlString = xmlString + "</timetable>";
        writeFile(xmlString);
    }
    
    /**
     * This method is use to write the contents into the specified file whose path is given by erasing the previous contents in file!!!
//     * @param filePath : The String specifying the path of file.
     * @param message : String containing the message that has to be write into the file.
     */
    public void writeFile(String message){
        try(PrintWriter writer = new PrintWriter(new FileOutputStream(new File(filePath)),true))
        {
            writer.println(message);
        }
        catch (IOException ie){
            System.out.println("Exception in writing of the file:"+ie.getMessage());
        }
    }
    
    /**
     * This method is use to read the file which path is given and return the contents of the file!!!
//     * @param filePath : The String specifying the path of file.
     * @return : String containing the final contents of the file read.
     */
    public String readFile(){
        String str,outputString = "";
        try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(filePath)))))
        {
            while((str=br.readLine())!=null)
            {
                    outputString = outputString + str;
            }
        }
        catch (IOException ie){
            System.out.println("Error while reading the file:"+ie.getMessage());
            return null;
        }
        return outputString;//When the exception comes the "" blank string will be returned!!!
    }
    
    public Day[] parseTimeTable(Map<String,Teacher> teachers){
        Day[] timeTable = new Day[8];//6+2extra lec
        /**
         * So every timeTable has 6 days and every 6 days has 6+2 lectures!!!
         */
        for(int i=0;i<8;i++){
            timeTable[i] = new Day();
        }
        
        String xmlFileContents = readFile(),content = "";
        System.out.println("Content :"+xmlFileContents);
        if(xmlFileContents == null){
            return null;
        }
        String regex = "<day>((\\s.*?)*)<\\/day>";//Regex for the each day contents!!!
        Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);//MULTILINE because it goes till end of the entire input sequence and not just till \n which is default!!!
        final Matcher matcher = pattern.matcher(xmlFileContents);
        while (matcher.find()) {
            content = matcher.group(1);//Will contain the each day part!!!
            regex = "<dayNo>(.*?)<\\/dayNo>";//Regex for each day number!!!
            Pattern dayNoPattern = Pattern.compile(regex, Pattern.MULTILINE);
            Matcher dayNoMatcher = dayNoPattern.matcher(content);
            //dayNoMatcher will always contain one because there will always be only one <dayNo> tag!!!
            int dayNo = -1, lectureNo = -1;
            if(dayNoMatcher.find())
                dayNo = Integer.parseInt(dayNoMatcher.group(1));
            
            regex = "<lecture>\\s*<lectureNo>(.*?)<\\/lectureNo>\\s*(<thoery>|<practical>)\\s*((\\s*?.*?)*)\\s*(<\\/thoery>|<\\/practical>)\\s*<\\/lecture>";
            pattern = Pattern.compile(regex, Pattern.MULTILINE);
            Matcher lectureMatcher = pattern.matcher(content);
            while(lectureMatcher.find()){
                lectureNo = Integer.parseInt(lectureMatcher.group(1));
                if("<practical>".equals(lectureMatcher.group(2))){
                    //It is practical Lecture!!!
//                    content = ;
                    regex = "(<a>|<b>|<c>)((.*?\\s*?)*)<teacher>((.*?\\s*?)*)</teacher>((.*?\\s*?)*)(</a>|</b>|</c>)";//1,2,4
                    pattern = Pattern.compile(regex, Pattern.MULTILINE);
                    Matcher practicalMatcher = pattern.matcher(lectureMatcher.group(3));
                    ArrayList<Teacher> allBatchTeachers = new ArrayList<>();
                    ArrayList<String> allBatchSubjects = new ArrayList<>();
                    while(practicalMatcher.find()){//Only one!!!
                        String batchIndicator = practicalMatcher.group(1);
//                        String subjectName = practicalMatcher.group(2).replaceAll("\\s", "");
//                        String teacherName = practicalMatcher.group(4).replaceAll("\\s", "");
                        String subjectName = practicalMatcher.group(2).trim();
                        String teacherName = practicalMatcher.group(4).trim();
//                        System.out.println("TEacher NAme:"+teacherName);
//                        if(teachers != null)
                            teachers.get(teacherName).setLectureAllocated(lectureNo, dayNo);
//                        if("<a>".equals(batchIndicator))
                        allBatchTeachers.add(teachers.get(teacherName));
                        allBatchSubjects.add(subjectName);
//                        else if("<b>".equals(batchIndicator)){
//                            allBatchTeachers.add(teachers.get(teacherName));
//                            allBatchSubjects.add(subjectName);
//                        }
//                        else if("<c>".equals(batchIndicator))
                        
//                            ;                        
                    }
                    int size = allBatchTeachers.size();
                    switch (size) {
                        case 1:
                            ((Day)timeTable[dayNo]).addPracticalLecture(lectureNo, new PraticalLecture(allBatchSubjects.get(0), allBatchTeachers.get(0)));
                            break;
                        case 2:
                            ((Day)timeTable[dayNo]).addPracticalLecture(lectureNo, new PraticalLecture(allBatchSubjects.get(0),allBatchSubjects.get(1), allBatchTeachers));
                            break;
                        case 3:
                            ((Day)timeTable[dayNo]).addPracticalLecture(lectureNo, new PraticalLecture(allBatchSubjects.get(0),allBatchSubjects.get(1),allBatchSubjects.get(2), allBatchTeachers));
                            break;
                    }
                }else{
                    //It is theory Lecture!!!
                    regex = "(.*?)\\s*<teacher>\\s*(.*?)\\s*</teacher>";//Group 1 and 2!!!
                    pattern = Pattern.compile(regex, Pattern.MULTILINE);
                    Matcher theoryMatcher = pattern.matcher(lectureMatcher.group(3));
                    if(theoryMatcher.find()){//Only one!!!
//                        String subjectName = theoryMatcher.group(1);
//                        String teacherName = theoryMatcher.group(2);
                        Teacher teacherObj = teachers.get(theoryMatcher.group(2));
                        teacherObj.setLectureAllocated(lectureNo, dayNo);
                        ((Day)timeTable[dayNo]).addThoeryLecture(lectureNo, new TheoryLecture(theoryMatcher.group(1), teacherObj));
                    }
                }
//                lectureMatcher.group(4);//This is helper group!!!
            }
                    
//            System.out.println("Full match: " + matcher.group(0));
//            for (int i = 1; i <= matcher.groupCount(); i++) {
//                System.out.println("Group " + i + ": " + matcher.group(i));
//            }
//            content = matcher.group(2);
//            final Pattern pattern1;
//            final Matcher matcher1;
//            if(matcher.group(1).equals("<theory>")){
//                regex = "(.*?)\\s*<teacher>\\s*(.*?)\\s*</teacher>";//Group 1 and 2!!!
//                pattern1 = Pattern.compile(regex, Pattern.MULTILINE);
//                
//            }else if(matcher.group(1).equals("<practical>")){
//                
//            }
        }
        System.out.println("Rewturn kiya bhau!!");
        return timeTable;
    }
    
//    <lecture>\s*(<thoery>|<practical>)\s*((\s*?.*?)*)\s*(<\/thoery>|<\/practical>)\s*<\/lecture>
//    (<a>|<b>|<c>)((.*?\s*?)*)<teacher>((.*?\s*?)*)<\/teacher>((.*?\s*?)*)(<\/a>|<\/b>|<\/c>)
//    public static void main(String args){
//        Day[] days = new XMLParser("./src/automatictimetablegenerator/CO1.xml").parseTimeTable();
//    }
}

    