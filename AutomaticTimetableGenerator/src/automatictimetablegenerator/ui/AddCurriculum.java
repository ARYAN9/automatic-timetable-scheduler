/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.ui;

import automatictimetablegenerator.backend.Day;
import automatictimetablegenerator.backend.Teacher;
import automatictimetablegenerator.backend.XMLParser;
import automatictimetablegenerator.db.MySQLConnect;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Aryan Dhami
 */
public class AddCurriculum extends javax.swing.JFrame {

    private String oldWeekValue = "";
    private static final String WEEK_PLACEHOLDER = "Enter number of weeks";
    private final Connection conn;
    private ResultSet rs = null;
    private PreparedStatement ps = null;
    private final Map<String,Integer> mappingOfBranchNameToId;
    private final Map<String,Teacher> teachers;
    private final Map<String,Day[]> timeTables;

    //This HashMap is created so on the submit of the Form the branch id can be send!!! So,again we don't have to hit the Database!!!

    /**
     * Creates new form AddCurriculum
     */
    public AddCurriculum() {
        initComponents();
        mappingOfBranchNameToId = new HashMap<>();
        teachers = new ConcurrentHashMap<>();
        timeTables = new ConcurrentHashMap<>();
        conn = MySQLConnect.getConnection();
        initializeBranches();
        initializeAllTeachers();
//        initializeAllPreviousTimetables();
//        new Thread(() -> this.initializeAllPreviousTimetables(true)).start();
//        new Thread(this::initializeAllPreviousTimetables).start();
    }

    private void initializeBranches(){
        try{
            String sqlQuery = "SELECT * FROM branches";
            ps = conn.prepareStatement(sqlQuery);
            rs = ps.executeQuery();
            while(rs.next()){
                String branchName = rs.getString("name");
                branch_combobox.addItem(branchName);   
                mappingOfBranchNameToId.put(branchName, rs.getInt("id"));                
            }
        }catch(SQLException e){
            System.out.println("SQLException while loading subjects from DB: " + e.getMessage());
        }  
    }
    
    private void initializeAllTeachers(){
        try{
            String sqlQuery = "SELECT name FROM teachers";//Teachers from all the branches!!!
            ps = conn.prepareStatement(sqlQuery);            
            rs = ps.executeQuery();
            while(rs.next()){
                teachers.put(rs.getString("name"),new Teacher(rs.getString("name")));
            }
        }catch(SQLException e){
            System.out.println("SQLException while loading subjects from DB: " + e.getMessage());
        }

    }
    
    private void initializeAllPreviousTimetables(int selectedSemesterNumber){
        
//        Day[] days1 = new XMLParser("./src/automatictimetablegenerator/CO1.xml").parseTimeTable(teachers);
//        if(days1 == null){
//            System.out.println("Yes");
//            return;
//        }
        
        int numOfProcessors = Runtime.getRuntime().availableProcessors();
//        ExecutorService executor = Executors.newFixedThreadPool(numOfProcessors);
        ExecutorService executor = Executors.newCachedThreadPool();
        ArrayList<String> allBranches = new ArrayList<>(mappingOfBranchNameToId.keySet());
        CountDownLatch latch = new CountDownLatch(allBranches.size()*2);
        System.out.println("allBranches.size()*6:"+allBranches.size()*2);
//        final int i;
        allBranches.stream().forEach((String branch) -> {
            if((selectedSemesterNumber & 1) == 1){
                //Odd sem!!!
                //So, now all odd sems leaving this sem should be traversed!!!
                IntStream.iterate(1,  i -> i + 2).limit(3).forEach( i ->{
                    if(selectedSemesterNumber != i){
                        executor.submit(() -> {
                            System.out.println("Anita123:"+branch+i);
                            Day[] days = new XMLParser("./src/automatictimetablegenerator/"+branch+i+".xml").parseTimeTable(teachers);
                            if(days != null) //Because the NULL Values are not allowed in the ConcurrentHashMap due to ambiquity!!!
                                timeTables.put(branch+i,days);
                            System.out.println("latch.getCount():"+latch.getCount()+"branch:"+branch+i);
                            latch.countDown();
                        });
                    }
                });
            }else{
                //Even sem!!!
                //So, now all even sems leaving this sem should be traversed!!!
                IntStream.iterate(2,  i -> i + 2).limit(3).forEach( i ->{
                    if(selectedSemesterNumber != i){
                        executor.submit(() -> {
                            System.out.println("Anita:"+branch+i);
                            Day[] days = new XMLParser("./src/automatictimetablegenerator/"+branch+i+".xml").parseTimeTable(teachers);
                            if(days != null) //Because the NULL Values are not allowed in the ConcurrentHashMap due to ambiquity!!!
                                timeTables.put(branch+i,days);
                            System.out.println("latch.getCount():"+latch.getCount()+"branch:"+branch+i);
                            latch.countDown();
                        });
                    }
                });                
            }
        });
        executor.shutdown();// To make the ExecutorService stop accepting new tasks!!! 
        //To terminate the executor and remove it from waiting for new threads!!!
        try {
//            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            latch.await();
            System.out.println("latch.getCount():"+latch.getCount());
        } catch (InterruptedException E) {
            System.out.println("Some exception in executor service all threads not finished!!!");
        }

        System.out.println("Branch123:"+Arrays.toString(timeTables.get("CO1")));
        for(String branch:allBranches){
            for(int k=1;k<=6;k++){
                if(timeTables.get(branch+k) != null)
                {
                    System.out.println("Branch:"+branch+k);
                    for(int i=0;i<6;i++){
                        for(int j=0;j<8;j++){     
                            System.out.println("Day"+i+" :"+((Day)((timeTables.get(branch+k))[i])).getLecture(j));
                        }                                            
                    }
                }
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel10 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        semester_combobox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        num_weeks_tf = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        next_btn = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        branch_combobox = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        timetable_logo = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();

        jLabel10.setText("jLabel10");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(227, 232, 236));

        semester_combobox.setBackground(new java.awt.Color(72, 28, 95));
        semester_combobox.setForeground(new java.awt.Color(72, 28, 95));
        semester_combobox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-----------Select Semester-----------", "Sem 1", "Sem 2", "Sem 3", "Sem 4", "Sem 5", "Sem 6", "" }));
        semester_combobox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(72, 28, 95), 2));
        semester_combobox.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        semester_combobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                semester_comboboxActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Raleway Medium", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(72, 28, 95));
        jLabel1.setText("Semester               :");

        jLabel2.setFont(new java.awt.Font("Raleway Medium", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(72, 28, 95));
        jLabel2.setText("Number of weeks :");

        num_weeks_tf.setBackground(new java.awt.Color(227, 232, 236));
        num_weeks_tf.setFont(new java.awt.Font("Raleway Medium", 0, 14)); // NOI18N
        num_weeks_tf.setForeground(new java.awt.Color(72, 28, 95));
        num_weeks_tf.setText("Enter number of weeks");
        num_weeks_tf.setBorder(null);
        num_weeks_tf.setCaretColor(new java.awt.Color(72, 28, 95));
        num_weeks_tf.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                num_weeks_tfFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                num_weeks_tfFocusLost(evt);
            }
        });
        num_weeks_tf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                num_weeks_tfActionPerformed(evt);
            }
        });
        num_weeks_tf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                num_weeks_tfKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                num_weeks_tfKeyTyped(evt);
            }
        });

        jSeparator1.setBackground(new java.awt.Color(72, 28, 95));

        jPanel2.setBackground(new java.awt.Color(227, 232, 236));
        jPanel2.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 3, new Color(72,28,95)));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 9, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 347, Short.MAX_VALUE)
        );

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/automatictimetablegenerator/ui/images/close.png"))); // NOI18N
        jLabel9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
        });

        next_btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/automatictimetablegenerator/ui/images/next.png"))); // NOI18N
        next_btn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        next_btn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                next_btnMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                next_btnMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                next_btnMouseExited(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Raleway SemiBold", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(72, 28, 95));
        jLabel12.setText("Timetable schedular");

        jSeparator2.setBackground(new java.awt.Color(72, 28, 95));

        jPanel3.setBackground(new java.awt.Color(227, 232, 236));
        jPanel3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel3MouseClicked(evt);
            }
        });

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/automatictimetablegenerator/ui/images/AddTeacher.png"))); // NOI18N

        jLabel6.setFont(new java.awt.Font("Raleway Medium", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(72, 28, 95));
        jLabel6.setText("Add teacher");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel5))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(227, 232, 236));
        jPanel4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel4MouseClicked(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/automatictimetablegenerator/ui/images/AddSubject.png"))); // NOI18N

        jLabel7.setFont(new java.awt.Font("Raleway Medium", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(72, 28, 95));
        jLabel7.setText("Add subject");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel7))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        branch_combobox.setBackground(new java.awt.Color(72, 28, 95));
        branch_combobox.setForeground(new java.awt.Color(72, 28, 95));
        branch_combobox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-----------Select Branch-----------" }));
        branch_combobox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(72, 28, 95), 2));
        branch_combobox.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        branch_combobox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                branch_comboboxActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Raleway Medium", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(72, 28, 95));
        jLabel11.setText("Branch                  :");

        jPanel5.setBackground(new java.awt.Color(227, 232, 236));
        jPanel5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel5MouseEntered(evt);
            }
        });

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/automatictimetablegenerator/ui/images/Sub-Teacher.png"))); // NOI18N

        jLabel14.setFont(new java.awt.Font("Raleway Medium", 0, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(72, 28, 95));
        jLabel14.setText("Teacher->Branch");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel14))
        );

        jPanel8.setBackground(new java.awt.Color(227, 232, 236));
        jPanel8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 88, Short.MAX_VALUE)
        );

        jPanel6.setBackground(new java.awt.Color(227, 232, 236));
        jPanel6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel6MouseClicked(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Raleway Medium", 0, 12)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(72, 28, 95));
        jLabel17.setText("<html>View Previous<br>Timetable</html>");

        timetable_logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/automatictimetablegenerator/ui/images/Schedule.png"))); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(timetable_logo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(timetable_logo)
                .addGap(7, 7, 7)
                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jPanel7.setBackground(new java.awt.Color(227, 232, 236));
        jPanel7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel7MouseClicked(evt);
            }
        });

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/automatictimetablegenerator/ui/images/AddBranch.png"))); // NOI18N

        jLabel19.setFont(new java.awt.Font("Raleway Medium", 0, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(72, 28, 95));
        jLabel19.setText("Add branch");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel19)
                .addGap(11, 11, 11))
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addComponent(jLabel18)
                .addGap(17, 17, 17)
                .addComponent(jLabel19)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(219, 340, Short.MAX_VALUE)
                                .addComponent(jLabel3)
                                .addGap(148, 148, 148))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(150, 150, 150)
                                        .addComponent(jLabel8))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(47, 47, 47)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(next_btn)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel1)
                                                    .addComponent(jLabel2)
                                                    .addComponent(jLabel11))
                                                .addGap(80, 80, 80)
                                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(branch_combobox, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(semester_combobox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(num_weeks_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel12)
                        .addGap(0, 0, 0)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(semester_combobox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(15, 15, 15)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(num_weeks_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2))
                                .addGap(3, 3, 3)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(branch_combobox, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11))
                        .addGap(47, 47, 47)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(next_btn)
                        .addGap(45, 45, 45)
                        .addComponent(jLabel3)
                        .addGap(59, 59, 59))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void num_weeks_tfFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_num_weeks_tfFocusGained
        // TODO add your handling code here:
        String tempString = num_weeks_tf.getText();
        if("".equals(tempString) || tempString.equals(WEEK_PLACEHOLDER)){
            num_weeks_tf.setText("");    
        }  
    }//GEN-LAST:event_num_weeks_tfFocusGained

    private void num_weeks_tfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_num_weeks_tfActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_num_weeks_tfActionPerformed

    private void num_weeks_tfFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_num_weeks_tfFocusLost
        // TODO add your handling code here:
        if("".equals(num_weeks_tf.getText())){
           num_weeks_tf.setText(WEEK_PLACEHOLDER);            
        }
    }//GEN-LAST:event_num_weeks_tfFocusLost

    private void semester_comboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_semester_comboboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_semester_comboboxActionPerformed

    private void num_weeks_tfKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_num_weeks_tfKeyTyped
        // TODO add your handling code here:
        System.out.println("getModi"+evt.getModifiers());        
        System.out.println("getModi1"+evt.getKeyCode());
        System.out.println("getModi2"+KeyEvent.VK_BACK_SPACE);
        if(evt.isControlDown()){
            System.out.println("triple:"+num_weeks_tf.getText());
            num_weeks_tf.setText(oldWeekValue);
            evt.consume();
            System.out.println("jalassskjsandkask");
            return;
        }
        if (!((evt.getKeyChar() >= '0' && evt.getKeyChar() <= '9') || (evt.getModifiers()==KeyEvent.CTRL_MASK))) {
            System.out.println("evt.getKeyChar():"+evt.getKeyChar());            
            System.out.println("evt.getKeyChar():"+evt.getKeyCode());
            System.out.println("evt.getKeyChar()1:"+evt.getModifiers());            
            System.out.println("evt.getKeyChar()2:"+KeyEvent.CTRL_MASK);


            System.out.println("evt.getKeyChar():"+KeyEvent.VK_CONTROL);

            evt.consume();  
            oldWeekValue = num_weeks_tf.getText();
            System.out.println("oldWeekvalu update:"+oldWeekValue);

            return;
        }        
            oldWeekValue = oldWeekValue + evt.getKeyChar();
            System.out.println("oldWeekvalu update1:"+oldWeekValue+"11:"+num_weeks_tf.getText());
            System.out.println("wowiwiwiwi:"+evt.getKeyChar());

    }//GEN-LAST:event_num_weeks_tfKeyTyped

    private void num_weeks_tfKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_num_weeks_tfKeyPressed
        // TODO add your handling code here:
//        System.out.println("getNodi"+evt.getModifiers());        
//        System.out.println("getNodi1"+evt.getKeyCode());
//        System.out.println("getNodi2"+KeyEvent.VK_BACK_SPACE);
//        if(evt.getKeyCode() == KeyEvent.VK_BACK_SPACE){
//            System.out.println("Indie");
//            if(!"".equals(oldWeekValue)){
//                System.out.println("Indw111");
//                oldWeekValue.substring(0, oldWeekValue.length()-1);
//                
//            }
//        }
    }//GEN-LAST:event_num_weeks_tfKeyPressed

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jLabel9MouseClicked

    private void next_btnMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_next_btnMouseEntered
        // TODO add your handling code here:
        next_btn.setIcon(new ImageIcon("./src/automatictimetablegenerator/ui/images/Next_hover.png"));
    }//GEN-LAST:event_next_btnMouseEntered

    private void next_btnMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_next_btnMouseExited
        // TODO add your handling code here:
        next_btn.setIcon(new ImageIcon("./src/automatictimetablegenerator/ui/images/next.png"));
    }//GEN-LAST:event_next_btnMouseExited

    private void branch_comboboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_branch_comboboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_branch_comboboxActionPerformed

    private void next_btnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_next_btnMouseClicked
        // TODO add your handling code here:
        //Some validations!!!
        if(branch_combobox.getSelectedIndex() == 0){
            JOptionPane.showMessageDialog(this, "Please select the branch!!!");
        }else if("".equals(num_weeks_tf.getText()) || num_weeks_tf.getText().equals(WEEK_PLACEHOLDER)){
            JOptionPane.showMessageDialog(this, "Please enter the number of weeks!!!");
        }else if(semester_combobox.getSelectedIndex() == 0){
            JOptionPane.showMessageDialog(this, "Please select the semester!!!");
        }else{
            String selectedSemester = semester_combobox.getSelectedItem().toString();
            int semesterNumber = Integer.parseInt(selectedSemester.substring(selectedSemester.length()-1));
            int numberOfWeeks = Integer.parseInt(num_weeks_tf.getText());

//            -----------Select Semester-----------, Sem 1, Sem 2, Sem 3, Sem 4, Sem 5, Sem 6,  
//            this.initializeAllPreviousTimetables(semesterNumber);
            Thread threadObj = new Thread(() -> this.initializeAllPreviousTimetables(semesterNumber));
            threadObj.start();
            try {
                threadObj.join();
            } catch (InterruptedException ex) {
                System.out.println("InterruptedException while waiting for thread!!!");
            }
//            else
//                new Thread(() -> this.initializeAllPreviousTimetables(false)).start();
            java.awt.EventQueue.invokeLater(() -> {                
                new AddSubjects(semesterNumber,numberOfWeeks,mappingOfBranchNameToId.get(branch_combobox.getSelectedItem().toString()),teachers).setVisible(true);
            });
        }
    }//GEN-LAST:event_next_btnMouseClicked

    private void jPanel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseClicked
        // TODO add your handling code here:
        dispose();
        java.awt.EventQueue.invokeLater(() -> {
            new ViewTimetable(new HashSet<>(mappingOfBranchNameToId.keySet()),timeTables,teachers).setVisible(true);
        });
    }//GEN-LAST:event_jPanel6MouseClicked

    private void jPanel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseClicked
        // TODO add your handling code here:
        new AddTeacher(new HashMap<>(mappingOfBranchNameToId)).setVisible(true);
    }//GEN-LAST:event_jPanel3MouseClicked

    private void jPanel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseClicked
        // TODO add your handling code here:
        new AddSubject(new HashSet<>(mappingOfBranchNameToId.keySet()), new HashMap<>(mappingOfBranchNameToId)).setVisible(true);
    }//GEN-LAST:event_jPanel4MouseClicked

    private void jPanel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseClicked
        // TODO add your handling code here:
        System.out.println("In");
        new NewAssignTeacherToBranch(new HashSet<>(teachers.keySet()),new HashSet<>(mappingOfBranchNameToId.keySet())).setVisible(true);
    }//GEN-LAST:event_jPanel5MouseClicked

    private void jPanel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel7MouseClicked
        // TODO add your handling code here:
        java.awt.EventQueue.invokeLater(() -> {
            new AddBranch().setVisible(true);
        });
    }//GEN-LAST:event_jPanel7MouseClicked

    private void jPanel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel5MouseEntered

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddCurriculum.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddCurriculum.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddCurriculum.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddCurriculum.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AddCurriculum().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> branch_combobox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel next_btn;
    private javax.swing.JTextField num_weeks_tf;
    private javax.swing.JComboBox<String> semester_combobox;
    private javax.swing.JLabel timetable_logo;
    // End of variables declaration//GEN-END:variables
}
