/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.ui;

import automatictimetablegenerator.db.MySQLConnect;
import java.awt.Color;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Aryan Dhami
 */
public class Login extends javax.swing.JFrame {

    /**
     * Creates new form Login
     */
    public Login() {
        initComponents();        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        username_tf = new javax.swing.JTextField();
        password_tf = new javax.swing.JTextField();
        next_btn_panel = new javax.swing.JPanel();
        main_frame_loginBtn = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel1.setBackground(new java.awt.Color(247, 247, 247));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/automatictimetablegenerator/ui/images/timetable.jpeg"))); // NOI18N

        jLabel12.setFont(new java.awt.Font("Raleway Medium", 0, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(72, 28, 95));
        jLabel12.setText("<html><b>Samay Mapper</b> <br>(An Automatic Timetable Schedular)</html>");

        jSeparator2.setBackground(new java.awt.Color(72, 28, 95));

        jLabel2.setFont(new java.awt.Font("Raleway SemiBold", 1, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(72, 28, 95));
        jLabel2.setText("Login");

        username_tf.setBackground(new java.awt.Color(247, 247, 247));
        username_tf.setForeground(new java.awt.Color(72, 28, 95));
        username_tf.setText("Aryan");
        username_tf.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Enter Username", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Raleway Medium", 0, 14), new java.awt.Color(72, 28, 95))); // NOI18N

        password_tf.setBackground(new java.awt.Color(247, 247, 247));
        password_tf.setForeground(new java.awt.Color(72, 28, 95));
        password_tf.setText("19821");
        password_tf.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Enter Password", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Raleway Medium", 0, 14), new java.awt.Color(72, 28, 95))); // NOI18N

        next_btn_panel.setBackground(new java.awt.Color(247, 247, 247));
        next_btn_panel.setForeground(new java.awt.Color(58, 188, 255));

        main_frame_loginBtn.setBackground(new java.awt.Color(247, 247, 247));
        main_frame_loginBtn.setFont(new java.awt.Font("Raleway SemiBold", 0, 14)); // NOI18N
        main_frame_loginBtn.setForeground(new java.awt.Color(72, 28, 95));
        main_frame_loginBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        main_frame_loginBtn.setText("Login");
        main_frame_loginBtn.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(72, 28, 95)));
        main_frame_loginBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        main_frame_loginBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                main_frame_loginBtnMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                main_frame_loginBtnMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                main_frame_loginBtnMouseExited(evt);
            }
        });

        javax.swing.GroupLayout next_btn_panelLayout = new javax.swing.GroupLayout(next_btn_panel);
        next_btn_panel.setLayout(next_btn_panelLayout);
        next_btn_panelLayout.setHorizontalGroup(
            next_btn_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, next_btn_panelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(main_frame_loginBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        next_btn_panelLayout.setVerticalGroup(
            next_btn_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(main_frame_loginBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jLabel9.setBackground(new java.awt.Color(247, 247, 247));
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/automatictimetablegenerator/ui/images/Close1.png"))); // NOI18N
        jLabel9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel12)
                                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(133, 133, 133)
                                .addComponent(jLabel2)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(password_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(username_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(next_btn_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(41, 41, 41)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(536, 536, 536)
                        .addComponent(jLabel9))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(username_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(password_tf, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(next_btn_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 8, Short.MAX_VALUE)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(32, 32, 32))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void main_frame_loginBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_main_frame_loginBtnMouseClicked
        // TODO add your handling code here:
        processLogin();
    }//GEN-LAST:event_main_frame_loginBtnMouseClicked

    private void main_frame_loginBtnMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_main_frame_loginBtnMouseEntered
        // TODO add your handling code here:
        next_btn_panel.setBackground(new Color(72,28,95));//getRGB(58,188,255)
        main_frame_loginBtn.setForeground(new Color(247,247,247));
    }//GEN-LAST:event_main_frame_loginBtnMouseEntered

    private void main_frame_loginBtnMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_main_frame_loginBtnMouseExited
        // TODO add your handling code here:
        next_btn_panel.setBackground(new Color(247,247,247));
        main_frame_loginBtn.setForeground(new Color(72,28,95));
    }//GEN-LAST:event_main_frame_loginBtnMouseExited

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jLabel9MouseClicked

    
    private void processLogin(){
        if("".equals(username_tf.getText()))
            JOptionPane.showMessageDialog(null, "Please enter the username!!!");
        else if("".equals(password_tf.getText()))
            JOptionPane.showMessageDialog(null, "Please enter the password!!!");
        else{
            /**
             * Check from the database that the username password is correct or not!!!
             */
            try{
                Connection conn = MySQLConnect.getConnection();
                String sqlQuery = "SELECT * FROM teacher_list WHERE username = ? AND password = ?";
                PreparedStatement ps = conn.prepareStatement(sqlQuery);
                ps.setString(1, username_tf.getText());
                ps.setString(2, password_tf.getText());
                ResultSet rs = ps.executeQuery();
                if(rs.next()){
                    /*Means the username and password is correct!!!*/
                    dispose();/*Disposing this Login Frame!!!*/
                    Loading loadingObj = new Loading();
                    loadingObj.setVisible(true);/*Displaying our loading part!!!*/
                    /**
                     * The below we are creating one helper thread that will last for 2800 milliseconds 
                     * and after that time will stop our 'Loading' part and display the Add Curriculum page!!!
                     */
                    new Thread(() -> {
                        try{
                            Thread.sleep(2800);
                        }catch(InterruptedException e){
                            System.out.println("Some Exception occured while sleeping the thread!!!");
                        }
                        loadingObj.dispose();
                        java.awt.EventQueue.invokeLater(() -> {
                            new AddCurriculum().setVisible(true);
                        });
                    }).start();
                }
                else
                    JOptionPane.showMessageDialog(null, "Username/Password is incorrect.Please try again!!!");                
            }catch(SQLException e){
                System.out.println("SQLException while loading subjects from DB: " + e.getMessage());
            } 
        }
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel main_frame_loginBtn;
    private javax.swing.JPanel next_btn_panel;
    private javax.swing.JTextField password_tf;
    private javax.swing.JTextField username_tf;
    // End of variables declaration//GEN-END:variables
}
