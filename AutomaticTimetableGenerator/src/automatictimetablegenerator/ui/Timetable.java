/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automatictimetablegenerator.ui;

import automatictimetablegenerator.backend.Day;
import automatictimetablegenerator.backend.Lecture;
import automatictimetablegenerator.backend.LectureConstants;
import automatictimetablegenerator.backend.Teacher;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.BoxLayout;

/**
 *
 * @author Aryan Dhami
 */
public class Timetable extends javax.swing.JFrame {
    private Day timeTable[];
    
    /**
     * Creates new form Timetable
     */
    public Timetable(Day timeTable[]) {
        initComponents();
        this.timeTable = timeTable;
        customInit();
//        updateUI();
    }
    
    private void customInit(){
        automatictimetablegenerator.backend.PraticalLecture praticalLecture = null;
        automatictimetablegenerator.backend.TheoryLecture theoryLecture = null;
        ArrayList<Teacher> teachers = null;
        
        timings_main_panel.setLayout(new BoxLayout(timings_main_panel,BoxLayout.Y_AXIS));
        timings_main_panel.add(new Timings("9:00","10:15"));
        timings_main_panel.add(new Timings("10:15","11:15"));
//        timings_main_panel.add(new Timings("11:15","11:30"));
        timings_main_panel.add(new Timings("11:30","12:30"));
        timings_main_panel.add(new Timings("12:30","1:30"));
        timings_main_panel.add(new Timings("2:00","3:00"));
        timings_main_panel.add(new Timings("3:00","4:00"));
        
        boolean flag = false;        
        int index;
        for(index=0;index<6;index++){
            Lecture lectureObj = ((Day)timeTable[index]).getLecture(6);
            if(lectureObj != null)
                break;
        }
        if(index != 6)
            timings_main_panel.add(new Timings("Extra","Lecture"));
        for(index=0;index<6;index++){
            Lecture lectureObj = ((Day)timeTable[index]).getLecture(7);
            if(lectureObj != null)
                break;
        }
        if(index != 6)
            timings_main_panel.add(new Timings("Extra","Lecture"));
        timings_main_panel.validate();
    
        timings_main_panel.updateUI();
        timings_main_panel.repaint();
        timings_main_panel.revalidate();
        System.out.println(timings_main_panel.isShowing());
        
        monday_panel.setLayout(new BoxLayout(monday_panel,BoxLayout.Y_AXIS));
        tuesday_panel.setLayout(new BoxLayout(tuesday_panel,BoxLayout.Y_AXIS));
        wednesday_panel.setLayout(new BoxLayout(wednesday_panel,BoxLayout.Y_AXIS));
        thrusday_panel.setLayout(new BoxLayout(thrusday_panel,BoxLayout.Y_AXIS));
        friday_panel.setLayout(new BoxLayout(friday_panel,BoxLayout.Y_AXIS));
//        saturday_panel.setLayout(new BoxLayout(saturday_panel,BoxLayout.Y_AXIS));
        saturday_panel.setLayout(new GridLayout(1,1));
        
        for(int i=0;i<6;i++){
            for(int j=0;j<8;j++){
                boolean isBBatch = false,isCBatch = false,isTheory = false;
//                System.out.println("Day"+i+" :"+((Day)timeTable[i]).getLecture(j));                
                Lecture lectureObj = ((Day)timeTable[i]).getLecture(j);
                /*The lecture can either be thory or pratical*/
                if(lectureObj != null){
                    if(lectureObj.getLectureType() == LectureConstants.PRACTICALS){
                        if((j & 1) == 1)//means the j is even!!!
                            continue;
                        /*Means the lecture is Pratical!!! Now,seeing is 1 batch,2 batch or 3 batch!!!*/
                        praticalLecture = (automatictimetablegenerator.backend.PraticalLecture)lectureObj;
                        teachers = praticalLecture.getTeachers();
                        int size = teachers.size();
                        if(size == 3){
                            isBBatch = true;
                            isCBatch = true;                    
                        }else if(size == 2)
                            isBBatch = true;                    
                    }else{
                        isTheory = true;
                        theoryLecture = (automatictimetablegenerator.backend.TheoryLecture)lectureObj;
                    }
                
                
                    switch (i) {
                        case 0:
                            if(isTheory)
                                monday_panel.add(new TheoryLecture(theoryLecture.getName(),theoryLecture.getTeacher().getName()));
                            else if(isBBatch && isCBatch)
                                monday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName(), praticalLecture.getCBatchName(), teachers.get(2).getName()));
                            else if(isBBatch)
                                monday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName()));
                            else
                                monday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName()));
                            
                            //                        praticalLecture.getaBatchName()
                            //                        monday_panel.add();
                            break;
//                    if(!isTheory)
//                        i++;
//                        i = i + 2;
                        case 1:
                            System.out.println("Ayaya");
                            if(isTheory)
                                tuesday_panel.add(new TheoryLecture(theoryLecture.getName(),theoryLecture.getTeacher().getName()));
                            else if(isBBatch && isCBatch)
                                tuesday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName(), praticalLecture.getCBatchName(), teachers.get(2).getName()));
                            else if(isBBatch)
                                tuesday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName()));
                            else
                                tuesday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName()));
                            break;
                        case 2:
                            if(isTheory)
                                wednesday_panel.add(new TheoryLecture(theoryLecture.getName(),theoryLecture.getTeacher().getName()));
                            else if(isBBatch && isCBatch)
                                wednesday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName(), praticalLecture.getCBatchName(), teachers.get(2).getName()));
                            else if(isBBatch)
                                wednesday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName()));
                            else
                                wednesday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName()));
                            break;
                        case 3:
                            if(isTheory)
                                thrusday_panel.add(new TheoryLecture(theoryLecture.getName(),theoryLecture.getTeacher().getName()));
                            else if(isBBatch && isCBatch)
                                thrusday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName(), praticalLecture.getCBatchName(), teachers.get(2).getName()));
                            else if(isBBatch)
                                thrusday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName()));
                            else
                                thrusday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName()));
                            break;
                        case 4:
                            if(isTheory)
                                friday_panel.add(new TheoryLecture(theoryLecture.getName(),theoryLecture.getTeacher().getName()));
                            else if(isBBatch && isCBatch)
                                friday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName(), praticalLecture.getCBatchName(), teachers.get(2).getName()));
                            else if(isBBatch)
                                friday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName()));
                            else
                                friday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName()));
                            break;
                        case 5:
                            if(isTheory)
                                saturday_panel.add(new TheoryLecture(theoryLecture.getName(),theoryLecture.getTeacher().getName()));
                            else if(isBBatch && isCBatch)
                                saturday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName(), praticalLecture.getCBatchName(), teachers.get(2).getName()));
                            else if(isBBatch)
                                saturday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName(), praticalLecture.getBBatchName(), teachers.get(1).getName()));
                            else
                                saturday_panel.add(new PraticalLecture(praticalLecture.getABatchName(), teachers.get(0).getName()));
                            break;
                        default:
                            break;
                    }
                }
                
            }
        }
              
        validate();
        revalidate();
        repaint();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        timings_main_panel = new javax.swing.JPanel();
        monday_panel = new javax.swing.JPanel();
        tuesday_panel = new javax.swing.JPanel();
        wednesday_panel = new javax.swing.JPanel();
        thrusday_panel = new javax.swing.JPanel();
        friday_panel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        saturday_panel = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        timetable_logo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(1889, 1804));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane1.setPreferredSize(new java.awt.Dimension(32767, 32767));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout timings_main_panelLayout = new javax.swing.GroupLayout(timings_main_panel);
        timings_main_panel.setLayout(timings_main_panelLayout);
        timings_main_panelLayout.setHorizontalGroup(
            timings_main_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 168, Short.MAX_VALUE)
        );
        timings_main_panelLayout.setVerticalGroup(
            timings_main_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        monday_panel.setBackground(new java.awt.Color(255, 255, 255));
        monday_panel.setPreferredSize(new java.awt.Dimension(263, 0));

        javax.swing.GroupLayout monday_panelLayout = new javax.swing.GroupLayout(monday_panel);
        monday_panel.setLayout(monday_panelLayout);
        monday_panelLayout.setHorizontalGroup(
            monday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 330, Short.MAX_VALUE)
        );
        monday_panelLayout.setVerticalGroup(
            monday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 572, Short.MAX_VALUE)
        );

        tuesday_panel.setBackground(new java.awt.Color(255, 255, 255));
        tuesday_panel.setPreferredSize(new java.awt.Dimension(263, 0));

        javax.swing.GroupLayout tuesday_panelLayout = new javax.swing.GroupLayout(tuesday_panel);
        tuesday_panel.setLayout(tuesday_panelLayout);
        tuesday_panelLayout.setHorizontalGroup(
            tuesday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 326, Short.MAX_VALUE)
        );
        tuesday_panelLayout.setVerticalGroup(
            tuesday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        wednesday_panel.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout wednesday_panelLayout = new javax.swing.GroupLayout(wednesday_panel);
        wednesday_panel.setLayout(wednesday_panelLayout);
        wednesday_panelLayout.setHorizontalGroup(
            wednesday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 263, Short.MAX_VALUE)
        );
        wednesday_panelLayout.setVerticalGroup(
            wednesday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        thrusday_panel.setBackground(new java.awt.Color(255, 255, 255));
        thrusday_panel.setPreferredSize(new java.awt.Dimension(263, 0));

        javax.swing.GroupLayout thrusday_panelLayout = new javax.swing.GroupLayout(thrusday_panel);
        thrusday_panel.setLayout(thrusday_panelLayout);
        thrusday_panelLayout.setHorizontalGroup(
            thrusday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 334, Short.MAX_VALUE)
        );
        thrusday_panelLayout.setVerticalGroup(
            thrusday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        friday_panel.setBackground(new java.awt.Color(255, 255, 255));
        friday_panel.setPreferredSize(new java.awt.Dimension(263, 0));

        javax.swing.GroupLayout friday_panelLayout = new javax.swing.GroupLayout(friday_panel);
        friday_panel.setLayout(friday_panelLayout);
        friday_panelLayout.setHorizontalGroup(
            friday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 323, Short.MAX_VALUE)
        );
        friday_panelLayout.setVerticalGroup(
            friday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel17.setFont(new java.awt.Font("Raleway SemiBold", 1, 18)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(72, 28, 95));
        jLabel17.setText("Thursday");

        jLabel16.setFont(new java.awt.Font("Raleway SemiBold", 1, 18)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(72, 28, 95));
        jLabel16.setText("Wednesday");

        jLabel15.setFont(new java.awt.Font("Raleway SemiBold", 1, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(72, 28, 95));
        jLabel15.setText("Tuesday");

        jLabel14.setFont(new java.awt.Font("Raleway SemiBold", 1, 18)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(72, 28, 95));
        jLabel14.setText("<html>Class<br>Time</html>");

        jLabel18.setFont(new java.awt.Font("Raleway SemiBold", 1, 18)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(72, 28, 95));
        jLabel18.setText("Friday");

        jLabel12.setFont(new java.awt.Font("Raleway SemiBold", 1, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(72, 28, 95));
        jLabel12.setText("Saturday");

        jLabel19.setFont(new java.awt.Font("Raleway SemiBold", 1, 18)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(72, 28, 95));
        jLabel19.setText("Monday");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(220, 220, 220)
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(165, 165, 165)
                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(176, 176, 176)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(183, 183, 183)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jLabel15)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)
                    .addComponent(jLabel12)
                    .addComponent(jLabel17))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        saturday_panel.setBackground(new java.awt.Color(255, 255, 255));
        saturday_panel.setPreferredSize(new java.awt.Dimension(263, 0));

        javax.swing.GroupLayout saturday_panelLayout = new javax.swing.GroupLayout(saturday_panel);
        saturday_panel.setLayout(saturday_panelLayout);
        saturday_panelLayout.setHorizontalGroup(
            saturday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 328, Short.MAX_VALUE)
        );
        saturday_panelLayout.setVerticalGroup(
            saturday_panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(470, 470, 470))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(timings_main_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(monday_panel, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tuesday_panel, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(wednesday_panel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(thrusday_panel, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(friday_panel, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(saturday_panel, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(390, 390, 390))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(monday_panel, javax.swing.GroupLayout.DEFAULT_SIZE, 572, Short.MAX_VALUE)
                            .addComponent(tuesday_panel, javax.swing.GroupLayout.DEFAULT_SIZE, 572, Short.MAX_VALUE)
                            .addComponent(wednesday_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(thrusday_panel, javax.swing.GroupLayout.DEFAULT_SIZE, 572, Short.MAX_VALUE)
                            .addComponent(friday_panel, javax.swing.GroupLayout.DEFAULT_SIZE, 572, Short.MAX_VALUE)
                            .addComponent(saturday_panel, javax.swing.GroupLayout.DEFAULT_SIZE, 572, Short.MAX_VALUE))
                        .addContainerGap())
                    .addComponent(timings_main_panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jScrollPane1.setViewportView(jPanel3);

        jLabel13.setFont(new java.awt.Font("Raleway SemiBold", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(72, 28, 95));
        jLabel13.setText("Your Timetable");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(timetable_logo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 1966, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(26, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(timetable_logo)
                    .addComponent(jLabel13))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 639, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Timetable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Timetable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Timetable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Timetable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//                new Timetable().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel friday_panel;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel monday_panel;
    private javax.swing.JPanel saturday_panel;
    private javax.swing.JPanel thrusday_panel;
    private javax.swing.JLabel timetable_logo;
    private javax.swing.JPanel timings_main_panel;
    private javax.swing.JPanel tuesday_panel;
    private javax.swing.JPanel wednesday_panel;
    // End of variables declaration//GEN-END:variables
}
